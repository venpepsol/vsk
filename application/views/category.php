<!--page title start-->
<style>
	.product-btn{
		padding: 17px 5px;
    text-align: center;
    font-size: 18px;
    color: #fff;
    background: #f05125;
	}
	.product-btn a{
		color: #fff;
	}
	.product-img{
		height:260px;
	}
</style>
<!--<section class="page-title o-hidden" data-bg-img="<?php echo base_url(); ?>themes/images/bg/02.jpg">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-12 col-md-12">
        <h1 class="mb-3">Product <span class="text-theme">Left sidebar</span></h1>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-4 justify-content-end">
            <li class="breadcrumb-item"><a href="index.html"><i class="fas fa-home"></i></a>
            </li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>products">Products</a>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section>-->

<!--page title end-->


<!--body content start-->

<div class="page-content">

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-md-12 order-lg-12">
        <div class="row mb-5 align-items-center">
          <div class="col-md-5">
            <!--<h5 class="text-capitalize mb-0">Showing 1 to 18 of 20 total</h5>-->
          </div>
          <div class="col-md-7 sm-mt-3 d-sm-flex justify-content-md-end align-items-center">
           
          </div>
        </div>
        <div class="row">
          <?php foreach($catlist as $row){ 
          if($row->category_type = 'main_category'){
		  	$urlLink = base_url().'products/'.$row->cat_slug;
		  }
		  else{
		  	$urlLink = base_url().'category/'.$row->cat_slug;
		  }
          
          ?>
          <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 mt-5">
            <div class="product-item">
              <div class="product-img">
               <a href="<?php echo $urlLink; ?>" class="product-name"> <img class="img-fluid" src="<?php echo base_url().$row->cat_img; ?>" alt=""></a>
               
              </div>
              <div class="product-desc"> <a href="<?php echo $urlLink; ?>" class="product-name">
                  <?php echo $row->category_name; ?>
                </a>
                
              </div>
              <div class="product-btn">
                <a href="<?php echo $urlLink; ?>" class="product-name"><?php echo $row->category_name; ?></a>
              </div>
            </div>
          </div>
<?php } ?>
          
        </div>        
       <!-- <nav aria-label="Page navigation" class="mt-5">
          <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-arrow-left"></i></a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">2</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a>
            </li>
            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-arrow-right"></i></a>
            </li>
          </ul>
        </nav>-->
      </div>
      <div class="col-lg-3 col-md-12 order-lg-1 sidebar">
       <?php $this->load->view("sidebar"); ?>
      </div>
    </div>
  </div>
</section>


<!--newsletter start--> 
<section class="theme-bg py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <h2 class="title mysliderbottom1">For <span>exclusive collections</span>, Get in touch with us now</h2>
      </div>
      <div class="col-lg-12 col-md-12 md-mt-3">
        <div class="subscribe-form">
          <div id="notes"></div>
          <form id="mc-form" class="group row align-items-center">
            <div class="col-sm-6">
            <input type="text" value="" name="enname" class="email " id="mc-name" placeholder="User Name" required="">
            </div>
            <div class="col-sm-6">
            <input type="email" value="" name="email" class="email " id="mc-email" placeholder="Email Address" required="">
            </div>
            <div class="col-sm-6">
            <input type="text" value="" name="mobile" class="email " id="mc-mobile" placeholder="Mobile No" required="">
            </div>
            <div class="col-sm-6">
            <input type="text" value="" name="message" class="email " id="mc-message" placeholder="Message" required="">
            </div>
            <div class="col-sm-4 xs-mt-1">
            <input class="btn btn-white btn-circle" type="submit" id="enquire" value="Enquire Now">
            </div>
            <label for="mc-email" class="subscribe-message"></label>            
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!--newsletter end--> 

</div>

<!--body content end-->
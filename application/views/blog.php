
        <!--Start breadcrumb area-->
        <section class="breadcrumb-area" style="background-image: url(<?php echo base_url()?>themes/images/resources/breadcrumb-bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title">
                                <h1>News & Updates</h1>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="<?php echo base_url();?>">Home</a></li>
                                    <li>Blog</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--End breadcrumb area-->
        <!--Start blog area-->
        <section id="blog-area" class="blog-default-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-colum-style1">
                            <!--Start single blog post-->
                            <div class="single-blog-post style3 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="img-holder">
                                    <img src="<?php echo base_url()?>themes/images/blog/v1-1.jpg" alt="Awesome Image">
                                    <div class="overlay-style-two"></div>
                                    <div class="overlay">
                                        <div class="box">
                                            <div class="post-date">
                                                <h3>Mar <br><span>02</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-holder">
                                    <h3 class="blog-title"><a href="javascript:(void0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
                                    <div class="meta-box">
                                        <ul class="meta-info">
                                            <li>By <a href="#">VSK</a></li>
                                            <li>In <a href="#">Completed</a></li>
                                        </ul>
                                    </div>
                                    <div class="text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                        <a class="btn-two" href="javascript:(void0);">Read More<span class="flaticon-next"></span></a>
                                    </div>
                                </div>
                            </div>
                            <!--End single blog post-->
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-colum-style1">
                            <!--Start single blog post-->
                            <div class="single-blog-post style3 wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                                <div class="img-holder">
                                    <img src="<?php echo base_url()?>themes/images/blog/v1-2.jpg" alt="Awesome Image">
                                    <div class="overlay-style-two"></div>
                                    <div class="overlay">
                                        <div class="box">
                                            <div class="post-date">
                                                <h3>Feb <br><span>14</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-holder">
                                    <h3 class="blog-title"><a href="javascript:(void0);">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></h3>
                                    <div class="meta-box">
                                        <ul class="meta-info">
                                            <li>By <a href="#">VSK</a></li>
                                            <li>In <a href="#">Completed</a></li>
                                        </ul>
                                    </div>
                                    <div class="text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                        <a class="btn-two" href="javascript:(void0);">Read More<span class="flaticon-next"></span></a>
                                    </div>
                                </div>
                            </div>
                            <!--End single blog post-->
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-colum-style1">
                            <!--Start single blog post-->
                            <div class="single-blog-post style3 wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                                <div class="img-holder">
                                    <img src="<?php echo base_url()?>themes/images/blog/v1-3.jpg" alt="Awesome Image">
                                    <div class="overlay-style-two"></div>
                                    <div class="overlay">
                                        <div class="box">
                                            <div class="post-date">
                                                <h3>Jan <br><span>31</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-holder">
                                    <h3 class="blog-title"><a href="javascript:(void0);">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></h3>
                                    <div class="meta-box">
                                        <ul class="meta-info">
                                            <li>By <a href="#">VSK</a></li>
                                            <li>In <a href="#">Completed</a></li>
                                        </ul>
                                    </div>
                                    <div class="text">
                                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit...</p>
                                        <a class="btn-two" href="javascript:(void0);">Read More<span class="flaticon-next"></span></a>
                                    </div>
                                </div>
                            </div>
                            <!--End single blog post-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End blog area-->
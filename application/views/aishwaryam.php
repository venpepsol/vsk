        <!--Start breadcrumb area-->
        <section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title">
                                <h1>VSK's Aishwaryam Apartment</h1>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="<?php echo base_url();?>">Home</a></li>
                                    <li><a href="<?php echo base_url();?>project">Projects</a></li>
                                    <li class="active">VSK's Aishwaryam Apartment</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->        
        <!--Start Project Description area-->
        <section class="project-description-area">
            <div class="pattern-bg wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                <img src="<?php echo base_url(); ?>themes/images/pattern/project-description-pattern.jpg" alt="Pattern Bg">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="project-description-image-box">
                            <img src="<?php echo base_url(); ?>themes/images/aishwaryam/img1.jpg" alt="Awesome Image">
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="project-description-content">
                            <div class="sec-title">
                                <p>Overview</p>
                                <div class="title">Sprawling across <br>9 acres,</div>
                            </div>
                            <div class="inner-content">
                                <p>The most spacious, densely green residential development, designed for childhood. With wide open spaces to run free, wander, explore, learn and grow. "Give your children a childhood they’ll cherish forever."</p>
                                <div class="bottpm-text">
                                    <p>Nestled between a lake and the east Kolkata wetlands, One10 is located in the prime neighborhood of Newtown, Action Area 1. It features an Olympic length swimming pool with water slides, Nature Trail, Multi-sport Game Courts,</p>
                                </div>
                                <ul>
                                    <li>Triple height Sports Arena,</li>
                                    <li>Centre for Extra-curricular activities,</li>
                                    <li>Montessori and Daycare.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Project Description area-->
        <!--Start Project Info Area-->
        <section class="project-info-area">
            <div class="pattern-bg wow slideInRight" data-wow-delay="100ms" data-wow-duration="1500ms">
                <img src="<?php echo base_url(); ?>themes/images/pattern/project-info-patten.jpg" alt="Pattern Bg">
            </div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6">
                        <div class="project-info-content">
                            <div class="project-info-title">
                                <h3>Project Info</h3>
                            </div>
                            <div class="inner-content">
                                <ul>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-calendar"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Status</h4>
                                            <span>Under Construction</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-architecture-and-city1"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Area</h4>
                                            <span>800 - 1000 Sq ft</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-maps-and-location1"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Location</h4>
                                            <span>Ramanathapuram</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-ruler"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Type</h4>
                                            <span>Villa</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-price"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Apartments</h4>
                                            <span>1200</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-architecture-and-city1"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Total Towers</h4>
                                            <span>6</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-company"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Flat Size</h4>
                                            <span>100 - 150 Sq ft</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-architecture-and-city1"></span>
                                        </div>
                                        <div class="title">
                                            <h4>Price Range</h4>
                                            <span>$260k - $700k</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="project-info-image-box">
                            <img src="<?php echo base_url(); ?>themes/images/aishwaryam/img2.jpg" alt="Awesome Image">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Project Info Area-->
        <!-- aminities -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="single-service-top text-center">
                        <div class="text">
                            <h2>Amenities</h2>
                            <div class="inner">
                                <p>ReHomes Realty brings to you the Building Revolution. It all started with realistic appraisal of your living needs.</p>
                            </div>
                        </div>
                    </div>
                    <div class="advantages-content">
                        <div class="row">
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/1.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Surveillance <br>System</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/2.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>24x7 <br>Security</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/3.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Firefighting <br>System</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/4.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Swimming <br>Pool</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/5.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Children's play <br>area</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/6.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Landscape <br>Garden</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/7.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>AC Community<br>Hall</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                            <!--Start Single Advantages Box-->
                            <div class="col-xl-3 col-lg-6 col-md-6">
                                <div class="single-advantages-box">
                                    <div class="inner">
                                        <div class="static-content">
                                            <div class="icon-holder">
                                                <img src="<?php echo base_url();?>themes/images/icon/8.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="title">
                                                <h3>Fitness <br>Center</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Single Advantages Box-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- aminities -->
        <!--Start Similar projects Area-->
        <section class="similar-projects-area">
            <div class="container-fluid similar-projects-content">
                <div class="similar-project-title text-center">
                    <h2>Plans</h2>
                </div>
                <div class="row">
                    <!--Start Single Similar Project-->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="single-similar-project">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/images/aishwaryam/1.jpg" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <span>VSK Aishwaryam</span>
                                <h3><a href="#">Plan1</a></h3>
                            </div>
                        </div>
                    </div>
                    <!--End Single Similar Project-->
                    <!--Start Single Similar Project-->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="single-similar-project">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/images/aishwaryam/2.jpg" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <span>VSK Aishwaryam</span>
                                <h3><a href="#">Plan2</a></h3>
                            </div>
                        </div>
                    </div>
                    <!--End Single Similar Project-->
                    <!--Start Single Similar Project-->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="single-similar-project">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/images/aishwaryam/3.jpg" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <span>VSK Aishwaryam</span>
                                <h3><a href="#">Plan3</a></h3>
                            </div>
                        </div>
                    </div>
                    <!--End Single Similar Project-->
                    <!--Start Single Similar Project-->
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                        <div class="single-similar-project">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/images/aishwaryam/4.jpg" alt="Awesome Image">
                            </div>
                            <div class="title-holder">
                                <span>VSK Aishwaryam</span>
                                <h3><a href="#">Plan4</a></h3>
                            </div>
                        </div>
                    </div>
                    <!--End Single Similar Project-->
                </div>
            </div>
        </section>
        <!--End Similar projects Area-->    

        <!--Start Video Image Holder Area-->
        <section class="video-image-holder-area mb-5 pb-5">
            <div class="similar-project-title text-center">
                <h2>Video Tours</h2>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="video-holder-box">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/images/aishwaryam/bg.jpg" alt="Awesome Image">
                                <div class="icon-holder">
                                    <div class="icon">
                                        <div class="inner text-center">
                                            <a class="html5lightbox wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" href="https://www.youtube.com/watch?v=qdLnNiRZa8U">
                                                <span class="flaticon-play-button"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Video Image Gallery Area-->

        <!--Start Instagram Area-->
        <section class="instagram-area">
            <div class="">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="instagram-title pl-5">
                            <!-- <div class="pattern-bg wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1500ms"></div> -->
                            <h3>Addresses</h3>
                            <p>Nanjundapuram road, Next to parson nestle, <br>Ramanathapuram, Tamil Nadu 641045.</p>
                            <h3>Transport</h3>
                            <ul>
                                <li><span class="fa fa-plane pr-3"></span>Airport 10km</li>
                                <li><span class="fa fa-train pr-3"></span>Podhanur Junction 2km</li>
                                <li><span class="fa fa-train pr-3"></span>Railway Junction 5km</li>
                                <li><span class="fa fa-bus pr-3"></span>Gandhipuram Bus Stand 4km</li>
                                <li><span class="fa fa-bus pr-3"></span>Singanallur Bus Stand 4km</li>
                                <li><span class="fa fa-shopping-bag pr-3"></span>Race Course 4km</li>
                            </ul>
                            <a class="btn-two mt-5" href="#" type="button" data-toggle="modal" data-target="#exampleModalCenter">Enquiry Now<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <ul class="instagram-items clearfix">
                            <li>
                                <div class="img-holder">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d709167.7842243555!2d76.87346497587548!3d10.915278361505344!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba859e5ab8ec365%3A0xdb55cad5c15c992a!2sVSK&#39;s%20ASHWATTHA%20Apartments!5e0!3m2!1sen!2sus!4v1622784766036!5m2!1sen!2sus" width="800" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--End Instagram Area-->        
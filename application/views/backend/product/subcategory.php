<section id="main" role="main">
        <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
		<?php 
			$displayback = "style='display:none;'";
			$displaynone = "style='display:block;'";
			if($subcatid > 0)
			{
				$displaynone = "style='display:none;'";
				$displayback = "style='display:block;'";
			}
		?>
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">Sub Category</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="<?php echo base_url(); ?>admin">Admin</a></li>
                        <li class="active">Sub Category</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
		<div id="note"></div>
		<div class="row" id="blogcatlist" <?php echo $displaynone; ?> >
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sub Category</h3>
								<div class="panel-title" style="text-align:right; padding:5px;">
									<button type="button" id="new_entry" class="btn btn-sm btn-primary btn-perspective">New Entry</button>
								</div>
                            </div>
                            <table class="table table-striped table-bordered table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Sub Category Name</th>
                                        <th>Category Name</th>
										<th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								 $i = 1;
								 foreach($subcategorylist as $row)
								{ ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $row->sub_cat_name; ?></td>
                                        <td><?php echo $row->category_name; ?></td>
										<td>
											<a class="btn btn-primary btn-xs btn-perspective" href="<?php echo base_url().'adminproduct/subcategory/'.$row->sub_cat_id; ?>"><i class="ico-pencil"></i></a>
											<button type="button" class="btn btn-success btn-xs btn-perspective delbtn" catid="<?php echo $row->sub_cat_id; ?>" ><i class=" ico-close"></i></button>
										</td>
                                    </tr>
									<?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
		<div class="row" id="blogcategory" <?php echo $displayback; ?>>
			<div class="col-md-12">
				<form id="categoryform" class="form-horizontal panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Category</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-3">Sub Category Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="subcategoryname" required="" value="<?php echo $subcategoryname; ?>">
								<input type="hidden" value="<?php echo $subcatid; ?>" id="subcatid"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Category</label>
							<div class="col-md-6">
								<select id="category" class="form-control selectizefield">
									<option value=""></option>
									<?php foreach($categorylist as $res)
									{
										echo '<option value="'.$res->cat_id.'">'.$res->category_name.'</option>';
									} ?>
								</select>
							</div>
						</div>
						<!--<div class="form-group">
							<label class="control-label col-md-3">Description</label>
							<div class="col-md-6">
								<div id="disnote"><?php echo $categorydesc; ?></div>
							</div>
						</div>-->
						<div class="form-group">
							<label class="control-label col-md-3">Image</label>
							<div class="col-md-6">
								<input type="file" id="productimg" name="productimg">
								<div class="alert alert-info">Please upload the image in same height and width (500px X 591px) for a better display in the website.</div>
								<textarea style="display:none;" id="oldimage">
									<?php if(isset($image))
									{
										echo $image;
									} ?>
								</textarea>
								<div id="imagepreview">
									<?php if(isset($image))
									{
										if($image != '')
										{
											echo '<image src="'.base_url().$image.'" height="100">';
										}
									} ?>
								</div>
							</div>
						</div>
						<div class="panel-footer text-center">
							<input type="submit" class="btn btn-primary" value="submit"/>
							<button type="reset" class="btn btn-inverse">Reset</button>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
$(document).ready(function(){
	$(".datatable").dataTable();
	$('#disnote').summernote({height: 250});
	$('.selectizefield').selectize();
	$("#new_entry").click(function()
	{
		$("#blogcatlist").css('display','none');
		$("#blogcategory").css('display','block');
	});
	var id = $("#subcatid").val();
	if(id > 0)
	{
		/*alert('<?php echo $catid; ?>');*/
		var category = $("#category")[0].selectize
		category.setValue('<?php echo $catid; ?>');
	}
});

$(".delbtn").click(function()
{
	var catid = $(this).attr("catid");
	if(catid > 0)
	{
		var confirmbox = confirm("Are You Sure To Remove This Sub Category");
		if(confirmbox)
		{
			var req = new Request();
			req.url = 'adminproduct/deletesubcategory';
			req.data = 
			{
				"catid": catid
			}
			RequestHandler(req,showmsg);
		}
	}
});
$("#categoryform").submit(function(e)
{
	e.preventDefault();
	
	
	var data = new FormData();
	var filelist = document.getElementById('productimg');
	if(filelist.files.length > 0)
	{
		for(var i=0; filelist.files.length > i; i++)
		{
			data.append('productimg', filelist.files[i]);
		}
		data.append('productimagechanged', 'yes');
	}
	else{
		data.append('productimg', '');
		data.append('productimagechanged','no');
	}
	data.append('categoryid',$('#category').val());
	data.append('subcategoryname',$('#subcategoryname').val());
	data.append('subcatid',$('#subcatid').val());
	data.append('oldimage',$('#oldimage').val());
	/*data.append('desc',$('#disnote').summernote('code'));*/
	
	var req = new Request();
	req.data = data;
	req.url = "adminproduct/savesubcategory";
	ImageRequestHandler(req,showmsg)
});
function showmsg(data)
{
	var mydata = JSON.parse(data);
	var str = '';
	if(mydata.iserror == false)
	{
		str += '<div class="alert alert-dismissable alert-success">';
		str += '<button aria-hidden="true" type="button" data-dismiss="alert" class="close">X</button>' ;
		str += mydata.msg+'</div>';
		setTimeout(function(){location.href='<?php echo base_url(); ?>adminproduct/subcategory'},1000);
	}
	else
	{
		str += '<div class="alert alert-dismissable alert-danger">';
		str += '<button class="close" type="button" aria-hidden="true" data-dismiss="alert">x</button>';
		str += mydata.msg+'</div>';
	}
	$("#note").html(str);
}

$('#productimg').change(function()
{
	readURL(this,'imagepreview')	
});

function readURL(input, previewDiv)
{
	if (input.files && input.files[0]) 
	{
		var reader = new FileReader();
		reader.onload = function (e) 
		{
			var str = "<div class='imagepreview' style='position:relative; float:left;'>";
			str += "<img src='"+e.target.result+"' class='images' style='height:100px;' />";
			str += "</div>";
			$("#"+previewDiv).html(str);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
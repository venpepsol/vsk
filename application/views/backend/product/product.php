
<section id="main" role="main">
<style>
	.relatedpreviewdiv
	{
		position: relative;
		float: left;
		margin-right: 5px;
		margin-bottom: 5px;
	}
	.relatedpreviewdiv span
	{
		position: absolute;
		top: -5px;
		right: -5px;
		background: red;
		border-radius: 50%;
		padding: 5px;
		color: #fff;
		font-size: 8px;
	}
	.displayblock{
		display: block;
	}
	.displaynone{
		display: none;
	}
</style>
	<div class="container-fluid">
	<?php 
		$displayblock = "style='display:none;'";
		$displaynone ="style='display:block;'";
		if($id > 0)
		{
			$displayblock = "style='display:block;'";
			$displaynone = "style='display:none;'";
		}
	 ?>
		<div class="page-header page-header-block">
			<div class="page-header-section">
				<h3 class="title semibold">Product</h3>
			</div>
			<div class="page-header-section">
				<div class="toolbar">
					<ol class="breadcrumb breadcrumb-transparent nm">
						<li><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
						<li class="active"> Product</li>
					</ol>
				</div>
			</div>
		</div>
		<div id="note"></div>
		<div class="row" id="productlist" <?php echo $displaynone;?> >
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Product List</h3>
						<div class="panel-title text-right">
							<button class="btn btn-primary btn-sm btn-perspective" id="newproduct">New Product</button>
						</div>
					</div>
					<div class="panel-body">
					<table class="table table-striped table-bordered table-hover datatable">
						<thead>
							<tr>
								<th>Sl No</th>
								<th>Product Title</th>
								<th>Product Code</th>
								<th>Category</th>
								<th>Sub Category</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i = 1;
						foreach($productlist as $row)
						{ ?>
							<tr>
								<td><?php echo $i; ?></td>	
								<td><?php echo $row->product_name; ?></td>
								<td><?php echo $row->product_code; ?></td>
								<td><?php echo $row->category_name; ?></td>
								<td><?php echo $row->sub_cat_name; ?></td>
								<td>
									<a class="btn btn-xs btn-primary btn-perspective" href="<?php echo base_url().'adminproduct/index/'.$row->product_id; ?>"><i class="ico-pencil"></i></a>
									<button productid="<?php echo $row->product_id; ?>" class="btn btn-xs btn-danger btn-perspective delbtn"><i class="ico-close"></i></button>
								</td>
							</tr>
						<?php $i++; } ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="productdetail" <?php echo $displayblock; ?> >
			<div class="col-md-12">
				<form class="form-horizontal panel panel-default" id="productform">
					<div class="panel-heading">
						<h3 class="panel-title">Poduct Detail</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-3">Poduct Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" required="" id="productname" value="<?php echo $productname; ?>"/>
								<input type="hidden" id="productid" value="<?php echo $id; ?>"/>
								<input type="hidden" id="subcatid" value="<?php echo $subcatid; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Poduct Code</label>
							<div class="col-md-6">
								<input maxlength="20" type="text" class="form-control" required="" id="productcode" value="<?php echo $productcode; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Category</label>
							<div class="col-md-6">
								<select id="category" class="form-control selectizefield">
									<option value=""></option>
									<?php foreach($categorys as $res)
									{
										echo '<option value="'.$res->cat_id.'">'.$res->category_name.'</option>';
									} ?>
								</select>
							</div>
						</div>
						<div class="form-group" id="subcatsec">
							<label class="control-label col-md-3">Sub Category</label>
							<div class="col-md-6">
								<select id="subcategory" class="form-control selectizefield">
									<option value=""></option>
									
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Image</label>
							<div class="col-md-6">
								<input type="file" id="relatedImages" name="relatedImages[]" multiple="" value="">
								<div class="alert alert-info">Please upload the image in same height and width (500px X 591px) for a better display in the website.</div>
								<textarea id="relatedImagesPath" name="relatedImagesPath" style="display: none;"><?php echo json_encode($catRelatedImages); ?></textarea>
								<div id="relatedImagesPreview">
	                            	<?php
	                            	if(count($catRelatedImages) > 0)
									{
										foreach($catRelatedImages as $row)
										{
											echo "<div class='relatedpreviewdiv'><img src=".base_url().$row->imagepath." height='70px' /><span class='ico-close delImg' curimg='".$row->imagepath."'></span></div>";
										}
									}
	                            	?>
	                            </div>
								
							</div>
						</div>
						<div class="form-group" id="specification">
		                            <label class="col-sm-3 control-label">
										Product Specification<span class="text-danger">*</span>
									</label>
		                            <div class="col-sm-6" id="multiplespec">
									<?php 
									$spe = 1;
									if($productspecification == "")
									{ ?>
									<div class="col-sm-6">
										<input type="text" id="productspeckey0" value="" class="form-control">
									</div>
									<div class="col-sm-6">
										<input type="text" id="productspecvalue0" value="" class="form-control">
									</div>
									<?php } else 
									{
										$spe = 0;
										foreach($productspecification as $row)
										{ ?>
											<div class="col-sm-6" style="margin-bottom: 7px;">
												<input type="text"  id="productspeckey<?php echo $spe; ?>" value="<?php echo $row->speckey; ?>" class="form-control">
											</div>
											<div class="col-sm-6" style="margin-bottom: 7px;">
												<input type="text"  id="productspecvalue<?php echo $spe; ?>" value="<?php echo $row->specvalue; ?>" class="form-control">
											</div>
										<?php $spe++; } } ?>
										<input type="hidden" value="<?php echo $spe; ?>" id="specvalue">
									
		                            </div>
									<div class="col-sm-1">
										 <input type="button" id="specbtnadd" class="form-control" value="+"/>
									</div>
									<div class="col-sm-1">
										 <input type="button" id="specbtnminus" class="form-control" value="-"/>
									</div>
									
		                        </div>
						<div class="form-group">
							<label class="control-label col-md-3">Description</label>
							<div class="col-md-6">
								<div id="disnote"><?php echo $productdesc; ?></div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="form-group text-center">
							<input type="submit" class="btn btn-primary" value="Submit">
							<button type="reset" class="btn btn-inverse">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
var oldrelatedimages = [];
var selectedimage = [];
var id = $("#productid").val();
var subcatfieldid = $("#subcatid").val();
$(document).ready(function()
{
	$('.datatable').dataTable();
	$('.selectizefield').selectize();
	$('#disnote').summernote({height: 250});
	
	
	
	
	if(id > 0)
	{
		var category = $("#category")[0].selectize
		category.setValue('<?php echo $catid; ?>');
		var subcategory = $("#subcategory")[0].selectize
		subcategory.setValue('<?php echo $subcatid; ?>');
		var oldImages = JSON.parse($("#relatedImagesPath").val());
		for(var i=0;i<oldImages.length;i++)
		{
			oldrelatedimages.push(oldImages[i].imagepath);
		}
	}
});
$(".uploadImage").change(function()
	{
		var previewDiv = $(this).attr('previewDiv');
		if(previewDiv != "")
		{
			readURL(this, previewDiv);
		}
	});
	$("#relatedImages").change(function(event)
	{
		if(id == "")
		$("#relatedImagesPreview").html("");
	    readURL_MultiImagePreview(this, 'relatedImagesPreview', 'related');
	});
function readURL_MultiImagePreview(input, previewDiv, imgtype)
	{
	    if (input.files && input.files[0])
	    {
	    	$.each(input.files, function(j)
	    	{
	    		var reader = new FileReader();
	    		reader.onload = function (e)
	    		{
	    			if(imgtype == "related")
	    			{
						selectedimage.push(input.files[j].name);
					}
					if(imgtype == "brand")
					{
						brand_selectedimage.push(input.files[j].name);
					}
	    			str = "<div class='relatedpreviewdiv'><img src='"+ e.target.result+"' height='70px' /><span class='ico-close delImg' imgType='"+imgtype+"' curimg='"+input.files[j].name+"'></span></div>";
	    			$("#"+previewDiv).append(str);
	            }
	            reader.readAsDataURL(this);
	        });
	    }
	}
	var k = $("#specvalue").val();
$('#specbtnadd').click(function()
{
	$('#multiplespec').append('<div class="col-sm-6" style="margin-top:7px;"><input type="text" id="productspeckey'+k+'" value="" class="form-control"></div><div style="margin-top:7px;" class="col-sm-6"><input type="text" id="productspecvalue'+k+'" value="" class="form-control"></div>');
	k++;
});
$('#specbtnminus').click(function()
{
	if(k>1)
	{
		$('#multiplespec div:last').remove();
		$('#multiplespec div:last').remove();
		k--;
	}
});
$(document).on("click",".delImg",function()
	{
		var imgType = $(this).attr("imgType");
		var curimage = $(this).attr("curimg");
		
		if(imgType == "related")
		{
			if(!$.inArray(curimage, oldrelatedimages) !== -1)
			{
				deletedImages.push(curimage);
			}
			
			selectedimage = jQuery.grep(selectedimage, function(value)
			{
				return value != curimage;
			});
			
			oldrelatedimages = jQuery.grep(oldrelatedimages, function(value)
			{
				return value != curimage;
			});
		}
		$(this).parents(".relatedpreviewdiv").remove();
	});	
$('#productimg').change(function()
{
	readURL(this,'imagepreview')	
});

function readURL(input, previewDiv)
{
	if (input.files && input.files[0]) 
	{
		var reader = new FileReader();
		reader.onload = function (e) 
		{
			var str = "<div class='imagepreview' style='position:relative; float:left;'>";
			str += "<img src='"+e.target.result+"' class='images' style='height:100px;' />";
			str += "</div>";
			$("#"+previewDiv).html(str);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

$("#newproduct").click(function()
{
	$("#productlist").css('display','none');
	$("#productdetail").css('display','block');
});

$('.delbtn').click(function()
{
	var productid = $(this).attr('productid');
	if(productid > 0)
	{
		var confirmbox = confirm("Are You Sure To Delete This Product");
		if(confirmbox)
		{
			var req = new Request();
			req.url = 'adminproduct/deleteproduct';
			req.data = 
			{
				"id" : productid
			}
			RequestHandler(req,showmsg);
		}
	}
});

$("#productform").submit(function(e)
{
	e.preventDefault();
	
	var data = new FormData();
	var productspec = [];
	
	/*var filelist = document.getElementById('productimg');
	if(filelist.files.length > 0)
	{
		for(var i=0; filelist.files.length > i; i++)
		{
			data.append('productimg', filelist.files[i]);
		}
		data.append('productimagechanged', 'yes');
	}
	else{
		data.append('productimg', '');
		data.append('productimagechanged','no');
	}*/
	for(var s=0; s<k; s++)
	{
		productspec.push({
			speckey :$("#productspeckey"+s).val(),
			specvalue :$("#productspecvalue"+s).val()
		}); 
	}
	
	var filesList =  document.getElementById("relatedImages");
	for (var ie = 0; ie< filesList.files.length; ie ++) 
	{
		data.append("relatedImages[]", filesList.files[ie]);
	}
	data.append( "selectedimage", JSON.stringify(selectedimage));
	data.append( "oldrelatedimages", JSON.stringify(oldrelatedimages));
	data.append('id',$('#productid').val());
	data.append('productname',$('#productname').val());
	data.append('productcode',$('#productcode').val());
	data.append('category',$('#category').val());
	data.append('subcategory',$('#subcategory').val());
	data.append('oldimage',$('#oldimage').val());
	data.append('desc',$('#disnote').summernote('code'));
	data.append( "productspec", JSON.stringify(productspec));
	/*alert(JSON.stringify(productspec));
	return;*/
	/*alert(oldrelatedimages);
	return;*/
	var req = new Request();
	req.data = data;
	req.url = "adminproduct/saveproduct";
	ImageRequestHandler(req,showmsg)
});

function showmsg(data)
{
	data = JSON.parse(data);
		
	var isError = data.isError;
	var msg = data.msg;
	
	var str = '';
	var redirectURL = '';
	
	if(isError)
	{
		str += '<div role="alert" class="alert alert-danger">';
		str += 'OOPS! ' + msg;
		str += '</div>';
	}
	else
	{
		str += '<div role="alert" class="alert alert-success">';
		str += 'WELL DONE! ' + msg;
		str += '</div>';
		
		redirectURL = '<?php echo base_url(); ?>adminproduct/';
	}
	$("#responseMsg").html(str);
	if(!isError)
	{
		setTimeout(function()
		{
			//location.href = redirectURL;
		},1000);
	}
}
$("#category").change(function()
	{
		var category = $(this).val();
		if(category != "")
		{
			var req = new Request();
			req.data = 
			{
				"category" : category
			};
			req.url = "adminproduct/subcategorylist";
			RequestHandler(req,subcateres);
		}
	});
function subcateres(data)
	{
		data = JSON.parse(data);
		var isError = data.iserror;
		var res = data.data;
		
		var selectize1 = $("#subcategory")[0].selectize
		selectize1.clearOptions();
		selectize1.destroy();
		
		var selectize2 = $("#subcategory").selectize();
		var selectize3 = $("#subcategory")[0].selectize
		alert(res[0].category_type);
		if(res[0].category_type == 'main_category'){
			$("#subcatsec").removeClass("displayblock");
			$("#subcatsec").addClass("displaynone");
		}
		else{
			$("#subcatsec").removeClass("displaynone");
			$("#subcatsec").addClass("displayblock");
		}
				
		if(isError == false)
		{
			if(res.length > 0)
			{
				for(var i=0; i<res.length; i++)
				{
					selectize3.addOption({
				         text: res[i].sub_cat_name,
				         value: res[i].sub_cat_id
				    });
				}
				selectize3.setValue('');
				selectize3.refreshItems();
				if(id > 0){
					selectize3.setValue(subcatfieldid);				
				}
			}
		}
	}
	
</script>
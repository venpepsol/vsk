  <section id="main" role="main">
            <!-- START Template Container -->
            <div class="container-fluid">
                <!-- Page Header -->
                <div class="page-header page-header-block">
                    <div class="page-header-section">
                        <h4 class="title semibold">Dashboard</h4>
                    </div>
                    <div class="page-header-section">
                        <!-- Toolbar -->
                        <div class="toolbar">
                            <ol class="breadcrumb breadcrumb-transparent nm">
                                <li><a href="<?php echo base_url(); ?>admin">Admin</a></li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                        <!--/ Toolbar -->
                    </div>
                </div>
<!-- START row -->
 				<div class="row">
                    <div class="col-sm-4">
                        <!-- START Statistic Widget -->
                        <div class="table-layout animation delay animating fadeInDown">
                            <div class="col-xs-4 panel bgcolor-info">
                                <div class="ico-image fsize24 text-center"></div>
                            </div>
                            <div class="col-xs-8 panel">
                                <div class="panel-body text-center">
                                    <h4 class="semibold nm"><?php echo count($catcnt); ?></h4>
                                    <p class="semibold text-muted mb0 mt5">Category</p>
                                </div>
                            </div>
                        </div>
                        <!--/ END Statistic Widget -->
                    </div>
                    <div class="col-sm-4">
                        <!-- START Statistic Widget -->
                        <div class="table-layout animation delay animating fadeInUp">
                            <div class="col-xs-4 panel bgcolor-teal">
                                <div class="ico-crown fsize24 text-center"></div>
                            </div>
                            <div class="col-xs-8 panel">
                                <div class="panel-body text-center">
                                    <h4 class="semibold nm"><?php echo count($prodcnt); ?></h4>
                                    <p class="semibold text-muted mb0 mt5">Product</p>
                                </div>
                            </div>
                        </div>
                        <!--/ END Statistic Widget -->
                    </div>
                    <div class="col-sm-4">
                        <!-- START Statistic Widget -->
                        <div class="table-layout animation delay animating fadeInDown">
                            <div class="col-xs-4 panel bgcolor-primary">
                                <div class="ico-users3 fsize24 text-center"></div>
                            </div>
                            <div class="col-xs-8 panel">
                                <div class="panel-body text-center">
                                    <h4 class="semibold nm">0</h4>
                                    <p class="semibold text-muted mb0 mt5">Enquiry</p>
                                </div>
                            </div>
                        </div>
                        <!--/ END Statistic Widget -->
                    </div>
                </div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Enquiry Details</h3>
							</div>
							<table class="table table-striped table-bordered table-hover datatable">
								<thead>
									<tr>
										<th>Sl No</th>
										<th>Name</th>
										<th>Phone No</th>
										<th>Email</th>
										<th>Product</th>
										<th>Subject</th>
										<th>Message</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$i = 1;
								foreach($contactform as $row)
								{ ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $row->username; ?></td>
										<td><?php echo $row->phoneno; ?></td>
										<td><?php echo $row->email; ?></td>
										<td><?php echo $row->productname; ?></td>
										<td><?php echo $row->subject; ?></td>
										<td><?php echo $row->message; ?></td>
										<td><?php echo $row->condate; ?></td>
									</tr>
									<?php $i++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
                <!--/ END row -->
		</div>
	</section>
<script>
$(document).ready(function()
{
	$(".datatable").dataTable();
});
</script>
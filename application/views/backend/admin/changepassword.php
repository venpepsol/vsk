<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('sitename'); ?></title>
        <meta name="author" content="<?php echo $this->config->item('sitename'); ?>">
        <meta name="description" content="<?php echo $this->config->item('sitename'); ?> ">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>theme/images/favicon.png">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        
        
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/library/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/layout.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/uielement.min.css">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="<?php echo base_url(); ?>backend/library/modernizr/js/modernizr.min.js"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                            <!--<span class="logo-figure inverse"></span>
                            <span class="logo-text inverse"></span>-->
                             <span>
                            <img style="margin-bottom:20px;" src="<?php echo base_url().'backend/image/logo.png';?>" width="40%" />
                            </span>
                            <h5 class="semibold text-muted mt-5">Login to your account.</h5>
                        </div>
                        <!--/ Brand -->

                       <hr><!-- horizontal line -->
                       
                       <div id="responseMsg"></div>
                       
                        <!-- Login form -->
                        <form class="panel" id="entryForm" data-parsley-validate>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input type="password" class="form-control" placeholder="Old Password" id="oldPassword" name="oldPassword" required="">
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input type="password" class="form-control" placeholder="New Password" id="newPassword" name="newPassword" required="">
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input type="password" class="form-control" placeholder="Retype Password" id="retypePassword" name="retypePassword" required="">
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block btn-success">Change Password</button>
                                </div>
                            </div>
                        </form>
                        <!-- Login form -->

                        <hr><!-- horizontal line -->

                        <!--<p class="text-muted text-center">Don't have any account? <a class="semibold" href="page-register.html">Sign up to get started</a></p>-->
                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/core/js/core.min.js"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/javascript/app.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/parsley/js/parsley.min.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/javascript/jquery.ajax.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/javascript/jquery.blockUI.js"></script>        
        <!--/ App and page level script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
    <!--/ END Body -->
<script>
	$("#entryForm").submit(function(e)
	{
		e.preventDefault();
		
		if ($('#entryForm').parsley().validate()) 
		{
			var oldPassword = $("#oldPassword").val();
			var newPassword = $("#newPassword").val();
			var retypePassword = $("#retypePassword").val();
			
			if(oldPassword != "" && newPassword != "" && retypePassword != "")
			{
				if(newPassword == retypePassword)
				{
					var req = new Request();
					req.data = 
					{
						"oldPassword" : oldPassword,
						"newPassword" : newPassword
					};
					req.url = "web/updatePassword";
					RequestHandler(req, showResponse);
				}
				else
				{
					alert('New Password Must Match With Retype Password.');
					return;
				}
			}
		}
	});
	
	function showResponse(data)
	{
		data = JSON.parse(data);
		
		var isError = data.isError;
		var msg = data.msg;
		
		var str = '';
		var redirectURL = '';
		
		if(isError)
		{
			str += '<div role="alert" class="alert alert-danger">';
			str += 'OOPS! ' + msg;
			str += '</div>';
		}
		else
		{
			str += '<div role="alert" class="alert alert-success">';
			str += 'WELL DONE! ' + msg;
			str += '</div>';
			
			redirectURL = '<?php echo base_url(); ?>admin/';
		}
		$("#responseMsg").html(str);
		if(!isError)
		{
			setTimeout(function()
			{
				location.href = redirectURL;
			},1000);
		}
	}
</script>
</html>
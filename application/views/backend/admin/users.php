<section id="main" role="main">
        <!-- START Template Container -->
    <div class="container-fluid">
        <!-- Page Header -->
		<?php 
			$displayback = "style='display:none;'";
			$displaynone = "style='display:block;'";
			if($userId > 0)
			{
				$displaynone = "style='display:none;'";
				$displayback = "style='display:block;'";
			}
		?>
        <div class="page-header page-header-block">
            <div class="page-header-section">
                <h4 class="title semibold">User</h4>
            </div>
            <div class="page-header-section">
                <!-- Toolbar -->
                <div class="toolbar">
                    <ol class="breadcrumb breadcrumb-transparent nm">
                        <li><a href="<?php echo base_url(); ?>admin">Admin</a></li>
                        <li class="active">User</li>
                    </ol>
                </div>
                <!--/ Toolbar -->
            </div>
        </div>
		<div id="note"></div>
		<div class="row" id="blogcatlist" <?php echo $displaynone; ?> >
		<!--<div class="alert alert-warning">
                                    <span class="semibold">Note :</span>&nbsp;&nbsp;<span id="user-note">Create a user</span>
                                </div>-->
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Users</h3>
								<div class="panel-title" style="text-align:right; padding:5px;">
									<button type="button" id="new_entry" class="btn btn-sm btn-primary btn-perspective">Create</button>
								</div>
                            </div>
                            <table class="table table-striped table-bordered table-hover datatable">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>User Type</th>
										<th>Manage</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								 $i = 1;
								 foreach($userList as $row)
								{ ?>
                                    <tr>
										<td><?php echo $i; ?></td>
                                        <td><?php echo $row->firstname; ?></td>
                                        <td><?php echo $row->email; ?></td>
                                        <td><?php echo $row->type; ?></td>
										<td>
											<a class="btn btn-primary btn-xs btn-perspective" href="<?php echo base_url().'admin/users/'.$row->userid; ?>"><i class="ico-pencil"></i></a>
											<button type="button" class="btn btn-success btn-xs btn-perspective delbtn" postId="<?php echo $row->userid; ?>" ><i class=" ico-close"></i></button>
										</td>
                                    </tr>
									<?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
		<div class="row" id="blogcategory" <?php echo $displayback; ?>>
			<div class="col-md-12">
				<form id="categoryform" class="form-horizontal panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Job Post</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label col-md-3">User Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="userTitle" required="" value="<?php echo $userTitle; ?>">
								<input type="hidden" value="<?php echo $userId; ?>" id="userId"/>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Email</label>
							<div class="col-md-6">
								<input type="email" class="form-control" id="email" required="" value="<?php echo $email; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" id="password"  >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Gender</label>
							<div class="col-md-6">
								<select id="gender" class="form-control selectizefield">
									<option value=""></option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">User Type</label>
							<div class="col-md-6">
								<select id="userType" class="form-control selectizefield">
									<option value=""></option>
									<option value="user">User</option>
									<option value="editor">Editor</option>
								</select>
							</div>
						</div>
						<div class="panel-footer text-center">
							<input type="submit" class="btn btn-primary" value="submit"/>
							<button type="reset" class="btn btn-inverse">Reset</button>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
$(document).ready(function(){
	$(".datatable").dataTable();
	$('.selectizefield').selectize();
	$('#disnote').summernote({height: 250});
	$("#new_entry").click(function()
	{
		$("#blogcatlist").css('display','none');
		$("#blogcategory").css('display','block');
	});
	var id = $("#userId").val();
	if(id > 0)
	{
		var userType = $("#userType")[0].selectize
		userType.setValue('<?php echo $userType; ?>');
		var gender = $("#gender")[0].selectize
		gender.setValue('<?php echo $gender; ?>');
	}
});

$(".delbtn").click(function()
{
	var postId = $(this).attr("postId");
	if(postId > 0)
	{
		var confirmbox = confirm("Are You Sure To Remove This User");
		if(confirmbox)
		{
			var req = new Request();
			req.url = 'admin/deleteusers';
			req.data = 
			{
				"postId": postId
			}
			RequestHandler(req,showmsg);
		}
	}
});
$("#categoryform").submit(function(e)
{
	e.preventDefault();
	var userId = $('#userId').val();
	var userTitle = $('#userTitle').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var gender = $('#gender').val();
	var usertype = $('#userType').val();
	if(userTitle != "" && email != "")
	{
		if(userId == '' && password == '')
		{
			alert("Please Fill Required Fields");
			return;
		}
		var req = new Request();
		req.data ={
			"userId":userId,
			"userTitle":userTitle,
			"email":email,
			"password":password,
			"gender":gender,
			"usertype":usertype
		}
		req.url = "admin/saveusers";
		RequestHandler(req,showmsg)
	}
	else{
		alert("Please Fill Required Fields");
	}
	
});
function showmsg(data)
{
	var mydata = JSON.parse(data);
	var str = '';
	if(mydata.isError == false)
	{
		str += '<div class="alert alert-dismissable alert-success">';
		str += '<button aria-hidden="true" type="button" data-dismiss="alert" class="close">X</button>' ;
		str += mydata.msg+'</div>';
		setTimeout(function(){location.href='<?php echo base_url(); ?>admin/users'},1000);
	}
	else
	{
		str += '<div class="alert alert-dismissable alert-danger">';
		str += '<button class="close" type="button" aria-hidden="true" data-dismiss="alert">x</button>';
		str += mydata.msg+'</div>';
	}
	$("#note").html(str);
}
</script>
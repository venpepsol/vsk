<!DOCTYPE html>
<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('sitename'); ?></title>
        <meta name="author" content="pampersdry.info">
        <meta name="description" content="Adminre is a clean and flat backend and frontend theme build with twitter bootstrap 3.1.1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>theme/img/favicon.png">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/selectize/css/selectize.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/datatables/css/jquery.datatables.min.css">
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/library/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/layout.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/uielement.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/summernote/summernote.css">
		
        <!--/ Application stylesheet -->
        
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="<?php echo base_url(); ?>backend/library/modernizr/js/modernizr.min.js"></script>
		 <!-- Library script : mandatory -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery-migrate.min.js"></script>
		 <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/bootstrap/js/bootstrap.min.js"></script>
        <!--/ END JAVASCRIPT SECTION -->
        
        <!--JQuery Plugins-->
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/summernote/summernote.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/selectize/js/selectize.min.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/datatables/js/jquery.datatables.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/datatables/tabletools/js/tabletools.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/datatables/tabletools/js/zeroclipboard.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/datatables/js/jquery.datatables-custom.min.js"></script>
		
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/parsley/js/parsley.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/jquery.ajax.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>backend/jquery.block.js"></script>
        <!--JQuery Plugins-->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
<style>
.mynavbar
{
	background : #fffefe;
}
.sidebar,.sidebar .topmenu{
	background: #334f9e;
}
.sidebar .topmenu li.active,.sidebar .topmenu li>.submenu{
	background-color: #f05125;
}
.sidebar .topmenu li.active a>.figure>[class*=" ico-"], .sidebar .topmenu li.active a>.figure>[class^=ico-]{
	color:#fff;
}
.container-fluid,html,.navbar-toolbar{
	background: #7d766ea6;
}
.sidebar .topmenu li>.submenu li a,.sidebar .topmenu li a,#header.navbar .navbar-toolbar .navbar-nav>li>a{
	color: #f1f1f1;
}
.sidebar .topmenu li:after{
	border: none;
}
</style>
        <!-- START Template Header -->
        <header id="header" class="navbar navbar-fixed-top">
            <!-- START navbar header -->
            <div class="navbar-header">
                <!-- Brand -->
                <a class="navbar-brand mynavbar" style="color:#fff;" href="<?php echo base_url(); ?>">
                    <!--<span class="logo-figure"></span>
                    <span class="logo-text"></span>-->
					<!--Life Spring360-->
					<img src="<?php echo base_url(); ?>backend/image/logo/backendlogo.png" style="width:60%;" alt="tricelygifts">
                </a>
                <!--/ Brand -->
            </div>
            <!--/ END navbar header -->

            <!-- START Toolbar -->
            <div class="navbar-toolbar clearfix">
                <!-- START Left nav -->
                <ul class="nav navbar-nav navbar-left">
                    <!-- Sidebar shrink -->
                    <li class="hidden-xs hidden-sm">
                        <a href="javascript:void(0);" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                            <span class="meta">
                                <span class="icon"></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Sidebar shrink -->

                    <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <li class="navbar-main hidden-lg hidden-md hidden-sm">
                        <a href="javascript:void(0);" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                            <span class="meta">
                                <span class="icon"><i class="ico-paragraph-justify3"></i></span>
                            </span>
                        </a>
                    </li>
                    <!--/ Offcanvas left -->

                   

                   
                </ul>
                <!--/ END Left nav -->

                <!-- START navbar form -->
                <div class="navbar-form navbar-left dropdown" id="dropdown-form">
                    <form action="" role="search">
                        <div class="has-icon">
                            <input type="text" class="form-control" placeholder="Search application...">
                            <i class="ico-search form-control-icon"></i>
                        </div>
                    </form>
                </div>
                <!-- START navbar form -->

                <!-- START Right nav -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Profile dropdown -->
                    <li class="dropdown profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="meta">
                            	<span class="avatar"><img src="<?php echo base_url(); ?>backend/image/default.jpg" class="img-circle" alt="" /></span>
                                <span class="text hidden-xs hidden-sm pl5"><?php echo $this->session->userdata('username'); ?></span>
                                <span class="caret"></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <!--<li><a href="<?php echo base_url();?>web/changepassword"><span class="icon"><i class="ico-exit"></i></span> Change Password</a></li>-->
							<li><a href="<?php echo base_url();?>admin/logout"><span class="icon"><i class="ico-exit"></i></span> Sign Out</a></li>
                        </ul>
                    </li>
                    <!-- Profile dropdown -->

                    <!-- Offcanvas right This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
                    <!--/ Offcanvas right -->
                </ul>
                <!--/ END Right nav -->
            </div>
            <!--/ END Toolbar -->
        </header>
        <!--/ END Template Header -->
<?php
$curpage = $this->uri->uri_string;
$editpage = explode('/',$curpage);
if(count($editpage) == 1)
{
	$curpage = $editpage[0];
}
elseif(count($editpage) == 2 || count($editpage) == 3)
{
	$curpage = $editpage[0].'/'.$editpage[1];
}
?>

<!-- START Template Sidebar (Left) -->
<aside class="sidebar sidebar-left sidebar-menu">
    <!-- START Sidebar Content -->
    <section class="content slimscroll">
        <!--<h5 class="heading">Main Menu</h5>-->
        <!-- START Template Navigation/Menu -->
        <ul class="topmenu topmenu-responsive" data-toggle="menu">
		<li>
            <a href="<?php echo base_url();?>web" data-parent=".topmenu">
                <span class="figure"><i class="ico-globe"></i></span>
                <span class="text">Web Preview</span>
            </a>
        </li>
        <?php
         if($this->session->userdata('usertype') == 'admin')
			 {
        if($curpage == "admin")
		{
			echo '<li class="active">';
		}
		else
		{
			echo '<li>';
		}
		?>
            <a href="<?php echo base_url();?>admin" data-parent=".topmenu">
                <span class="figure"><i class="ico-stats-up"></i></span>
                <span class="text">Dashboard</span>
            </a>
			<?php 
			 echo '</li>';
			 }
		?>
           <!-- <a href="<?php echo base_url();?>admin/users" data-parent=".topmenu">
                <span class="figure"><i class="ico-users"></i></span>
                <span class="text">User</span>
            </a>-->
		<?php
		if($curpage == "adminproduct")
		{
			echo '<li class="active">';
		}
		else
		{
			echo '<li>';
		} ?>
			<a href="javascript:void(0);" data-target="#blog" data-toggle="submenu" data-parent=".topmenu">
                            <span class="figure"><i class="ico-pencil5"></i></span>
                            <span class="text">Product</span>
                            <span class="arrow"></span>
                        </a>
                        <!-- START 2nd Level Menu -->
                        <ul id="blog" class="submenu collapse in">
                            <li class="submenu-header ellipsis">Product</li>
                            <li>
                                <a href="<?php echo base_url(); ?>adminproduct/category">
                                    <span class="text">Category</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>adminproduct/subcategory">
                                    <span class="text">Sub Category</span>
                                </a>
                            </li> 
							<li>
                                <a href="<?php echo base_url(); ?>adminproduct">
                                    <span class="text">Product</span>
                                </a>
                            </li>
                        </ul>
        <?php echo '</li>'; ?>
        </ul>
        <!--/ END Template Navigation/Menu -->
    </section>
    <!--/ END Sidebar Container -->
</aside>
     
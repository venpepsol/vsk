<!DOCTYPE html>

<html class="backend">
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('sitename'); ?> | Login Page</title>
        <meta name="description" content="A group of professionals, experienced in Direct Marketing, join hands to form Advanced Business Systems, a dedicated team which assists in building a strong business force.">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>backend/image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>backend/image/favicon.ico">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- Plugins stylesheet : optional -->
        
        
        <!--/ Plugins stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/library/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/layout.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>backend/stylesheet/uielement.min.css">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="<?php echo base_url(); ?>backend/library/modernizr/js/modernizr.min.js"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->

    <!-- START Body -->
    <body>
<style>
html body{
	background-image: url(backend/image/bg_v1.jpg);
	background-repeat: no-repeat;
    background-size: cover;
}
.btn-success {
    background-color: #eb7323;
    border-color: #eb7323;
}
	.logo-text.inverse
	{
		background-image: url("theme/images/logomenu.png");
	}
</style>
        <!-- START Template Main -->
        <section id="main" role="main">
            <!-- START Template Container -->
            <section class="container">
                <!-- START row -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <!-- Brand -->
                        <div class="text-center" style="margin-bottom:40px;">
                            
							 <span>
                            <img style="margin-bottom:20px;" src="<?php echo base_url().'backend/image/logo.png';?>" width="35%" />
                            </span>
                            <h5 class="semibold text-muted mt-5">Login to your account.</h5>
                        </div>
                        <!--/ Brand -->

                        <!-- Login form -->
                        <form class="panel" id="loginform">
                            <div class="panel-body">
                                <!-- Alert message -->
                                <div class="alert alert-warning">
                                    <span class="semibold">Note :</span>&nbsp;&nbsp;<span id="note">Enter your email and password</span>
                                </div>
                                <div class="form-group">
                                    <div class="form-stack has-icon pull-left">
                                        <input name="username" id="username" type="text" class="form-control input-lg" placeholder="Username / email" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your username / email" data-parsley-required>
                                        <i class="ico-user2 form-control-icon"></i>
                                    </div>
                                    <div class="form-stack has-icon pull-left">
                                        <input name="password" id="password" type="password" class="form-control input-lg" placeholder="Password" data-parsley-errors-container="#error-container" data-parsley-error-message="Please fill in your password" data-parsley-required>
                                        <i class="ico-lock2 form-control-icon"></i>
                                    </div>
                                </div>

                                <!-- Error container -->
                                <div id="error-container"class="mb15"></div>
                                <!--/ Error container -->

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="checkbox custom-checkbox">  
                                                <input type="checkbox" name="remember" id="remember" value="1">  
                                                <label for="remember">&nbsp;&nbsp;Remember me</label>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group nm">
                                    <button type="submit" class="btn btn-block btn-success"><span class="semibold">Sign In</span></button>
                                </div>
                            </div>
                        </form>
                        <!-- Login form -->

                       

                    </div>
                </div>
                <!--/ END row -->
            </section>
            <!--/ END Template Container -->
        </section>
        <!--/ END Template Main -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/jquery/js/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/library/core/js/core.min.js"></script>
        <!--/ Library script -->

        <!-- App and page level script -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/sparkline/js/jquery.sparkline.min.js"></script><!-- will be use globaly as a summary on sidebar menu -->
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/javascript/app.min.js"></script>
        
        
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/plugins/parsley/js/parsley.min.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>backend/javascript/pages/login.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>backend/jquery.ajax.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>backend/jquery.block.js"></script>
		<script>
			$("#loginform").submit(function(e)
			{
				
				e.preventDefault();
				if($("#loginform").parsley().validate())
				{
					var username = $("#username").val();
					var password = $("#password").val();
					
					var req = new Request();
					req.url = "login/checklogin";
					req.data = {
						"username":username,
						"password": password
					}
					RequestHandler(req,status);
				}
			});
			function status(data)
			{
				var mydata = JSON.parse(data);
				if(mydata.iserror)
				{
					$("#note").html(mydata.msg);
					$(".alert").removeClass("alert-warning");
					$(".alert").addClass("alert-danger");
				}
				else{
					$("#note").html(mydata.msg);
					$(".alert").removeClass("alert-warning");
					$(".alert").addClass("alert-success");
					setTimeout(function(){location.href='<?php echo base_url(); ?>admin'},1000);
				}
			}
		</script>
    </body>
    <!--/ END Body -->
</html>
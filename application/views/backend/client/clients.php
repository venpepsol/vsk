<section id="main" role="main">
	<!-- START Template Container -->
	<div class="container-fluid">
		<!-- Page Header -->
		<div class="page-header page-header-block">
			<div class="page-header-section">
				<h4 class="title semibold">
					Clients
				</h4>
			</div>
			<div class="page-header-section">
				<!-- Toolbar -->
				<div class="toolbar">
					<ol class="breadcrumb breadcrumb-transparent nm">
						<li><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
						<li class="active">Clients</li>
					</ol>
				</div>
				<!--/ Toolbar -->
			</div>
		</div>
		<div id="responseMsg"></div>
		<?php
		$displayblock = "style='display:none;'";
		$displaynone = "style='display:block;'";
		
		if($clientId != "")
		{
			$displayblock = "style='display:block;'";
			$displaynone = "style='display:none;'";
		}
		?>
		<!-- START row -->
		<div class="row" id="listDetails" <?php echo $displaynone; ?> >
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Clients</h3>
						<div class="panel-toolbar text-right">
							<button type="button" id="new_entry" class="btn btn-success btn-perspective">New Entry</button>
						</div>
					</div>
					<table class="table table-striped" id="datatable">
						<thead>
							<tr>
								<th>Client Name</th>
								<th>Client City</th>
								<th>Client State</th>
								<th>Client Image</th>
								<th>Manage</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($clientDetails) > 0)
							{
								foreach($clientDetails as $row)
								{
								?>
								<tr>
									<td><?php echo $row->client_name; ?></td>
									<td><?php echo $row->city; ?></td>
									<td><?php echo $row->state; ?></td>
									<td>
										<?php
										if($row->clientimg != "")
										{
										?>
										<img src="<?php echo base_url().$row->clientimg; ?>" height="75" width="75" />
										<?php
										}
										?>
									</td>
									<td>
										<button class="btn btn-primary btn-xs editClient" clientId="<?php echo $row->id; ?>">
											<span class="ico-pencil"></span>
										</button>
										<button class="btn btn-danger btn-xs delClient" clientId="<?php echo $row->id; ?>">
											<span class="ico-close"></span>
										</button>
									</td>
								</tr>
								<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/ END row -->
		<!-- START row -->
		<div class="row" id="entryDetails" <?php echo $displayblock; ?>>
			<div class="col-md-12">
				<!-- Form horizontal layout -->
				<form class="form-horizontal panel panel-default" id="clientForm" enctype="multipart/form-data" method="POST" data-parsley-validate>
					<div class="panel-heading">
						<h3 class="panel-title">Clients</h3>
					</div>               
					<div class="panel-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">
								Client Name<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<input type="text" id="clientName" required="" value="<?php echo $clientName; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								Client City<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<input type="text" id="clientCity" required="" value="<?php echo $clientCity; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								Client State<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<input type="text" id="clientState" required="" value="<?php echo $clientState; ?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3 ">
								Client Image<span class="text-danger">*</span>
							</label>
							<div class="col-sm-6">
								<input type="file" id="clientImage" name="clientImage" value="<?php echo $clientImage; ?>"><div class="alert alert-info">Please upload the image in same height and width (220*220) for a better display in the website.</div>
								<textarea id="clientImagePath" name="clientImagePath" style="display: none;"><?php echo $clientImage; ?></textarea>
								<div id="clientImagePreview">
	                            	<?php
	                            	if($clientImage != "")
									{
										echo "<img src=".base_url().$clientImage." height='100px' />";
									}
	                            	?>
	                            </div>
							</div>
						</div>
					</div>
					<div class="panel-footer text-center">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="reset" class="btn btn-inverse resetBtn">Reset</button>
					</div>
				</form>
				<!--/ Form horizontal layout -->
			</div>
		</div>
		<!--/ END row -->
	</div>
</section>

<script>
	
	$(document).ready(function()
	{
		$('#datatable').dataTable();
	});
	
	$("#new_entry").click(function()
	{
		$("#listDetails").css('display','none');
		$("#entryDetails").css('display','block');
	});
	
	$("#clientImage").change(function()
	{
		readURL(this);
	});
	
	function readURL(input)
	{
		if (input.files && input.files[0]) 
		{
			var reader = new FileReader();
			reader.onload = function (e) 
			{
				var str = "<img src='"+e.target.result+"' height='100px' />";
				$("#clientImagePreview").html(str);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#clientForm").submit(function(e)
	{
		e.preventDefault();
		
		if($('#clientForm').parsley().validate()) 
		{
			var clientId = '<?php echo $clientId; ?>';
			var clientName = $("#clientName").val();
			var clientCity = $("#clientCity").val();
			var clientState = $("#clientState").val();
			var oldClientImagePath = $("#clientImagePath").val();
			
			var data = new FormData();
			
			var filesList =  document.getElementById('clientImage');
			for (var ie = 0; ie< filesList.files.length; ie ++) 
			{
				data.append('clientImage', filesList.files[ie]);
			}
			
			data.append( "clientId", clientId);
			data.append( "clientName", clientName);
			data.append( "clientCity", clientCity);
			data.append( "clientState", clientState);
			data.append( "oldClientImagePath", oldClientImagePath);
			
			var req = new Request();
			req.data = data;
			req.url = "adminclient/saveClient";
			ImageRequestHandler(req, showResponse);
		}
	});
	
	$(".editClient").click(function()
	{
		var clientId = $(this).attr('clientId');
		if(clientId > 0)
		{
			location.href = '<?php echo base_url(); ?>adminclient/client/'+clientId;
		}
	});
	
	$(".delClient").click(function()
	{
		var clientId = $(this).attr('clientId');
		if(clientId > 0)
		{
			var bool = confirm("Are You Sure Want To Remove This Client?");
			if(bool)
			{
				var req = new Request();
				req.data = 
				{
					"clientId" : clientId
				};
				req.url = "adminclient/deleteClient";
				RequestHandler(req, showResponse);
			}
			else
			{
				return;
			}
		}
		else
		{
			return;
		}
	});

	function showResponse(data)
	{
		data = JSON.parse(data);
		var isError = data.isError;
		var msg = data.msg;
		var str = '';
		if(isError)
		{
			str = str + '<div class="alert alert-dismissable alert-danger">';
			str = str + '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>';
			str = str + '<strong>Oops! </strong>'+msg+'</div>';
		}
		else
		{
			str = str + '<div class="alert alert-dismissable alert-success">';
			str = str + '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>';
			str = str + '<strong>Success! </strong>'+msg+' </div>';
		}
		
		$("#responseMsg").html(str);
		
		if(!isError)
		{
			setTimeout(function()
			{
				location.href = '<?php echo base_url(); ?>adminclient/client';
			}, 1000);
		}
	}
	
	$(".resetBtn").click(function()
	{
		location.href = '<?php echo base_url(); ?>adminclient/client';
	});
	
</script>
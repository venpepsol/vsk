<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>VSK Housing India</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<link rel="icon" type="image/png" href="<?php echo base_url(); ?>themes/images/favicon/favicon-32x32.png" sizes="32x32">-->
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>themes/siteimg/favicon.ico" sizes="16x16">
    <!-- master stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/style.css">
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/custom.css">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>themes/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="<?php echo base_url(); ?>themes/js/html5shiv.js"></script>
    <![endif]-->

</head>

<body>
    <div class="boxed_wrapper">

        <div class="preloader"></div>

        <!-- Start Top Bar style2 -->
        <section class="top-bar-style2">
            <div class="top-style2 clearfix">
                <div class="top-style2-left">
                    <ul>
                        <li><a href="tel:+919585611222">+91 95856 11222</a></li>
                        <li><a href="mailto:gmcbe@vskhousingindia.com">gmcbe@vskhousingindia.com</a></li>
                    </ul>
                </div>
                <div class="top-style2-right">
                    <ul class="topbar-social-links">
                        <li><a href="https://www.facebook.com/VSKHOUSINGINDIA" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.instagram.com/vsk_housing_india/" target="_blank" ><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End Top Bar style2 -->

        <!--Start Main Header-->
        <header class="main-header header-style2 stricky">
            <div class="inner-container clearfix">
                <div class="logo-box-style2 float-left">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url(); ?>themes/siteimg/logo.png" alt="Awesome Logo">
                    </a>
                </div>
                <div class="main-menu-box float-right">
                    <nav class="main-menu style2 clearfix">
                        <div class="navbar-header clearfix">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        </div>
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="<?php echo base_url(); ?>">Home</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>about">About Us</a>
                                </li>
                                <li class="dropdown"><a href="<?php echo base_url(); ?>project">Projects</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>ongoing">Ongoing Project</a></li>
                                        <li><a href="<?php echo base_url(); ?>upcoming">Upcoming Project</a></li>
                                        <li><a href="<?php echo base_url(); ?>completed">Completed Project</a></li>
                                    </ul>
                                </li>
                                
                                <li><a href="<?php echo base_url(); ?>blog">Blog</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>faq">FAQ</a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                            </ul>
                        </div>
                    </nav>
                    <div class="mainmenu-right style2">
                        <div class="button">
                            <a class="btn-one" type="button" data-toggle="modal" data-target="#exampleModalCenter" href="#">Get a Quote<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--End Main Header-->
        
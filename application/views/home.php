<!--Main Slider-->
        <section class="main-slider style2">
            <div class="rev_slider_wrapper fullwidthbanner-container" id="rev_slider_one_wrapper" data-source="gallery">
                <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1">
                    <ul>
                        <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default"
                            data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?php echo base_url(); ?>themes/siteimg/slides/banner-1.jpg"
                            data-title="Slide Title" data-transition="parallaxvertical">

                            <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url(); ?>themes/siteimg/slides/banner-1.jpg">

                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['-80','-95','-80','-90']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="big-title">
                                        Create your<br> own comfort.
                                    </div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['25','0','-5','-20']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},
                        {"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]' style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="text">Optimise living space with furnishings of your taste.</div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['105','90','75','65']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="btn-box">
                                        <!--<a class="html5lightbox play-button" title="crystalo Video Gallery" href="javascript:void();">
                                            <span class="flaticon-arrow"></span>
                                        </a>-->
                                        <a class="slide-style2-button" type="button" data-toggle="modal" data-target="#exampleModalCenter" href="javascript:void();">Explore More <span class="flaticon-arrow"></span></a>
                                    </div>
                                </div>
                            </div>



                        </li>

                        <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default"
                            data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?php echo base_url(); ?>themes/siteimg/slides/banner-2.jpg"
                            data-title="Slide Title" data-transition="parallaxvertical">

                            <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url(); ?>themes/siteimg/slides/banner-2.jpg">

                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['-80','-95','-80','-90']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="big-title">
                                        Innovative ideas <br> of interiors
                                    </div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['25','0','-5','-20']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},
                        {"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]' style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="text">Fresher ambience will create a positive impact on your life.</div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['105','90','75','65']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="btn-box">
                                        <!--<a class="html5lightbox play-button" title="crystalo Video Gallery" href="javascript:void();">
                                            <span class="flaticon-arrow"></span>
                                        </a>-->
                                        <a class="slide-style2-button" type="button" data-toggle="modal" data-target="#exampleModalCenter" href="javascript:void();">Explore More <span class="flaticon-arrow"></span></a>
                                    </div>
                                </div>
                            </div>

                        </li>

                        <!--<li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default"
                            data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="<?php echo base_url(); ?>themes/images/slides/v2-3.jpg"
                            data-title="Slide Title" data-transition="parallaxvertical">

                            <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="<?php echo base_url(); ?>themes/images/slides/v2-3.jpg">

                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['-80','-95','-80','-90']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="big-title">
                                        Uniting design &<br> technology.
                                    </div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['25','0','-5','-20']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},
                        {"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]' style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="text">Creating lasting impressions through interior design.</div>
                                </div>
                            </div>
                            <div class="tp-caption" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingtop="[0,0,0,0]" data-responsive_offset="on" data-type="text" data-height="none" data-width="['800','800','700','400']" data-whitespace="normal"
                                data-hoffset="['15','15','15','15']" data-voffset="['105','90','75','65']" data-x="['left','left','left','left']" data-y="['middle','middle','middle','middle']" data-textalign="['top','top','top','top']" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                style="z-index: 7; white-space: nowrap;">
                                <div class="slide-content left-slide">
                                    <div class="btn-box">
                                        <a class="html5lightbox play-button" title="crystalo Video Gallery" href="https://www.youtube.com/watch?v=p25gICT63ek">
                                            <span class="flaticon-play-button"></span>
                                        </a>
                                        <a class="slide-style2-button" href="#">View Our Projects</a>
                                    </div>
                                </div>
                            </div>



                        </li>-->
                    </ul>
                </div>
            </div>
        </section>
        <!--End Main Slider-->

        <!--Start About Style2 Area-->
        <section class="about-style2-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="about-style2-text">
                            <div class="sec-title">
                                <p>About Company</p>
                                <div class="title">redefining<br/><span> lifestyle</span> of people</div>
                            </div>
                            <div class="text">
                                <p align="justify">VSK Housing India is one of the leading real estate development companies. having footprints in Coimbatore for last 25 years. The company has already delivered more than 2,50,000 sq. ft. and is currently executing several other real estate projects and specialized interior projects.</p>
                            </div>
                            <div class="authorised-info">
                                <!--<div class="signature">
                                    <img src="<?php echo base_url(); ?>themes/images/icon/signature.png" alt="Signature">
                                </div>-->
                                <div class="name">
                                    <h3>Mr. K.Peachiappan</h3>
                                    <p>Founder</p>
                                </div>
                            </div>
                            <div class="button">
                                <a class="btn-one" href="<?php echo base_url();?>about">More About Us<span class="flaticon-next"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="about-style2-image-box">
                            <div class="pattern wow slideInUp" data-wow-delay="100ms" data-wow-duration="1500ms"></div>
                            <div class="image">
                                <img src="<?php echo base_url(); ?>themes/siteimg/home/home-about.jpg" alt="Awesome Image">
                                <div class="overlay-box">
                                    <div class="title">
                                        <h1>25+ <span>Years Experience<br> in this Field</span></h1>
                                    </div>
                                    <div class="button">
                                        <a href="<?php echo base_url();?>about"><span class="icon-back"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End About Style2 Area-->
        
        <!--Start Working style2 Area-->
        <section class="working-style2-area" style="background-image:url(<?php echo base_url(); ?>themes/siteimg/home/ourwork-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="working-style2-content clearfix">
                            <!--Start Single Working Box Style2-->
                            <div class="single-working-box-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/ourwork-1.jpg" alt="Awesome Image">
                                    <div class="static-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-architecture-and-city"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Ongoing Projects</h3>
                                                        <div class="count">01</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="overlay-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-architecture-and-city"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Ongoing Projects</h3>
                                                        <div class="count">01</div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Which is the same as saying in<br> through shrrinkings from toil and<br> cases are perfectly.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more">
                                    <a href="<?php echo base_url();?>ongoing"><span class="icon-next"></span></a>
                                </div>
                            </div>
                            <!--End Single Working Box Style2-->
                            <!--Start Single Working Box Style2-->
                            <div class="single-working-box-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/ourwork-2.jpg" alt="Awesome Image">
                                    <div class="static-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-paint"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Upcoming Projects</h3>
                                                        <div class="count">02</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="overlay-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-paint"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Upcoming Projects</h3>
                                                        <div class="count">02</div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Which is the same as saying in<br> through shrrinkings from toil and<br> cases are perfectly.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more">
                                    <a href="<?php echo base_url(); ?>upcoming"><span class="icon-next"></span></a>
                                </div>
                            </div>
                            <!--End Single Working Box Style2-->
                            <!--Start Single Working Box Style2-->
                            <div class="single-working-box-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/ourwork-3.jpg" alt="Awesome Image">
                                    <div class="static-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-furniture-and-household"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Completed Projects</h3>
                                                        <div class="count">03</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="overlay-content">
                                        <div class="box">
                                            <div class="inner">
                                                <div class="icon-holder">
                                                    <span class="icon-furniture-and-household"></span>
                                                </div>
                                                <div class="text-holder">
                                                    <div class="title">
                                                        <h3>Completed Projects</h3>
                                                        <div class="count">03</div>
                                                    </div>
                                                    <div class="text">
                                                        <p>Which is the same as saying in<br> through shrrinkings from toil and<br> cases are perfectly.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="read-more">
                                    <a href="<?php echo base_url();?>completed"><span class="icon-next"></span></a>
                                </div>
                            </div>
                            <!--End Single Working Box Style2-->
                        </div>
                        <div class="working-style-bottom text-center">
                            <p>Wanna Work With Our Experienced Profesional Team? Make an Appointment.</p>
                            <div class="button">
                                <a class="btn-one" href="<?php echo base_url();?>project">View More<span class="flaticon-next"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Working style2 Area-->

        <!--Start Recently Project style2 Area-->
        <section class="recently-project-style2-area">
            <div class="container">
                <div class="sec-title text-center">
                    <p>What We Build</p>
                    <div class="title">Featured <span>Projects</span></div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="project-carousel-v2 owl-carousel owl-theme">
                            <!--Start single project style1-->
                            <div class="single-project-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/project-ashwattha.jpg" alt="Awesome Image">
                                    <div class="read-more">
                                        <a href="<?php echo base_url();?>ashwattha"><span class="icon-next"></span></a>
                                    </div>
                                    <div class="title-box">
                                        <h3>VSK’s Ashwattha Apartment</h3>
                                        <span>Ramanathapuram</span>
                                    </div>
                                </div>
                            </div>
                            <!--End single project style1-->
                            <!--Start single project style1-->
                            <div class="single-project-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/project-aishwaryam.jpg" alt="Awesome Image">
                                    <div class="read-more">
                                        <a href="<?php echo base_url();?>aishwaryam"><span class="icon-next"></span></a>
                                    </div>
                                    <div class="title-box">
                                        <h3>VSK Aishwaryam Apartment</h3>
                                        <span>Saravanampatti</span>
                                    </div>
                                </div>
                            </div>
                            <!--End single project style1-->
                            <!--Start single project style1-->
                            <div class="single-project-style2">
                                <div class="img-holder">
                                    <img src="<?php echo base_url(); ?>themes/siteimg/home/project-aksharam.jpg" alt="Awesome Image">
                                    <div class="read-more">
                                        <a href="<?php echo base_url();?>aksharam"><span class="icon-next"></span></a>
                                    </div>
                                    <div class="title-box">
                                        <h3>VSK’s Aksharam Apartment</h3>
                                        <span>KNG Pudur, GN Mills</span>
                                    </div>
                                </div>
                            </div>
                            <!--End single project style1-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Recently Project style2 Area-->
        
        <!--Start Why choose Area-->
        <section class="why-choose-area" style="background-image:url(<?php echo base_url(); ?>themes/images/parallax-background/why-choose-bg.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="why-choose-title float-left">
                            <div class="sec-title">
                                <div class="icon"><img src="<?php echo base_url(); ?>themes/siteimg/logo-em.png" width="100px" alt="VSK Logo"></div>
                                <div class="title">HAPPINESS LEADS <span><br/>TO SUCCESS</span></div>
                            </div>
                            <!--<ul>
                                <li>Well Considered Design</li>
                                <li>We Create For You</li>
                                <li>Leave The Details To Us</li>
                            </ul>-->
                            <div class="button">
                                <a class="btn-one" type="button" data-toggle="modal" data-target="#exampleModalCenter" href="<?php echo base_url(); ?>contact">Enquire Now<span class="flaticon-next"></span></a>
                            </div>
                        </div>
                        <div class="why-choose-content float-right">
                            <!--Start Single Box-->
                            <div class="single-box redbg">
                                <!--<div class="icon-holder">
                                    <span class="icon-scheme"></span>
                                </div>-->
                                <div class="text-holder">
                                    <h3>49+ Completed Project</h3>
                                    <p>Righteous indignations working beguileds all demoralized that blinded our works.</p>
                                </div>
                            </div>
                            <!--End Single Box-->
                            <!--Start Single Box-->
                            <div class="single-box whitebg">
                                <!--<div class="icon-holder">
                                    <span class="icon-guarantee-certificate1"></span>
                                </div>-->
                                <div class="text-holder">
                                    <h3>31+ Green Buildings</h3>
                                    <p>Have to be repudiated annoyances accepted The wise man therefore always holds.</p>
                                </div>
                            </div>
                            <!--End Single Box-->
                            <!--Start Single Box Mobile View-->
                            <div class="single-box blackbg mobileview">
                                <!--<div class="icon-holder">
                                    <span class="icon-wallet1"></span>
                                </div>-->
                                <div class="text-holder">
                                    <h3>Reasonable price</h3>
                                    <p>Our power of choice is untrammelled & when nothing prevents our being able.</p>
                                </div>
                            </div>
                            <!--End Single Box Mobile View-->
                            <!--Start Single Box-->
                            <div class="single-box whitebg">
                                <!--<div class="icon-holder">
                                    <span class="icon-hr1"></span>
                                </div>-->
                                <div class="text-holder">
                                    <h3>25+ years of experience</h3>
                                    <p>Rejects pleasures to secure other pleasures, endures pains to avoid worse.</p>
                                </div>
                            </div>
                            <!--End Single Box-->
                            <!--Start Single Box-->
                            <div class="single-box blackbg mobilehide">
                                <!--<div class="icon-holder">
                                    <span class="icon-wallet1"></span>
                                </div>-->
                                <div class="text-holder">
                                    <h3>Reasonable price</h3>
                                    <p>Our power of choice is untrammelled & when nothing prevents our being able.</p>
                                </div>
                            </div>
                            <!--End Single Box-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Why choose Area-->
        
        <!--Start Testimonial Style2 Area-->
        <section class="testimonial-style2-area">
            <div class="container">
                <div class="sec-title text-center">
                    <p>Testimonials</p>
                    <div class="title">Our Customer <span>Words</span></div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="testimonial-style2-content">
                            <div class="testimonial-carousel owl-carousel owl-theme">
                                <!--Start Single Testimonial style2-->
                                <div class="single-testimonial-style2 text-center">
                                    <div class="inner-content">
                                        <div class="static-content">
                                            <div class="quote-icon">
                                                <span class="icon-quote3"></span>
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">It’s our dream come true with VSK. A team with young and energetic people. A team focused on customer satisfaction without compromising the quality of the project. Wishing the entire team always a success for their upcoming projects.</p>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mr.V.Ramakrishnan</h3>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <div class="img-box">
                                                <img src="<?php echo base_url(); ?>themes/siteimg/icon/man.png" alt="Awesome Image">
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">It’s our dream come true with VSK. A team with young and energetic people. A team focused on customer satisfaction without compromising the quality of the project. Wishing the entire team always a success for their upcoming projects.</p>
                                                <div class="quote-icon">
                                                    <span class="icon-quote3"></span>
                                                </div>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mr.V.Ramakrishnan</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Single Testimonial style2 -->

                                <!--Start Single Testimonial style2-->
                                <div class="single-testimonial-style2 text-center">
                                    <div class="inner-content">
                                        <div class="static-content">
                                            <div class="quote-icon">
                                                <span class="icon-quote3"></span>
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">After a large search and heavy comparison and I come to a conclusion that finally found the best property for my dream home, "The VSK HOUSING INDIA". So good in construction and services are very excellent.</p>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mrs.M.Ramya Maheswari</h3>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <div class="img-box">
                                                <img src="<?php echo base_url(); ?>themes/siteimg/icon/woman.png" alt="Awesome Image">
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">After a large search and heavy comparison and I come to a conclusion that finally found the best property for my dream home, "The VSK HOUSING INDIA". So good in construction and services are very excellent.</p>
                                                <div class="quote-icon">
                                                    <span class="icon-quote3"></span>
                                                </div>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mrs.M.Ramya Maheswari</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Single Testimonial style2 -->

                                <!--Start Single Testimonial style2-->
                                <div class="single-testimonial-style2 text-center">
                                    <div class="inner-content">
                                        <div class="static-content">
                                            <div class="quote-icon">
                                                <span class="icon-quote3"></span>
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">It’s our dream come true with VSK. A team with young and energetic people. A team focused on customer satisfaction without compromising the quality of the project. Wishing the entire team always a success for their upcoming projects.</p>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mrs.N.Manjusha</h3>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <div class="img-box">
                                                 <img src="<?php echo base_url(); ?>themes/siteimg/icon/woman.png" alt="Awesome Image">
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">It’s our dream come true with VSK. A team with young and energetic people. A team focused on customer satisfaction without compromising the quality of the project. Wishing the entire team always a success for their upcoming projects.</p>
                                                <div class="quote-icon">
                                                    <span class="icon-quote3"></span>
                                                </div>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mrs.N.Manjusha</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Single Testimonial style2 -->
                                
                                <!--Start Single Testimonial style2-->
                                <div class="single-testimonial-style2 text-center">
                                    <div class="inner-content">
                                        <div class="static-content">
                                            <div class="quote-icon">
                                                <span class="icon-quote3"></span>
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">In fact this is my first time flat purchase where I am really happy to inform that "VSK HOUSING INDIA" extended their full support and serving to us in all angle.</p>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mr.G.Elango</h3>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <div class="img-box">
                                                 <img src="<?php echo base_url(); ?>themes/siteimg/icon/man.png" alt="Awesome Image">
                                            </div>
                                            <div class="text-box">
                                                <p align="justify">In fact this is my first time flat purchase where I am really happy to inform that "VSK HOUSING INDIA" extended their full support and serving to us in all angle.</p>
                                                <div class="quote-icon">
                                                    <span class="icon-quote3"></span>
                                                </div>
                                            </div>
                                            <div class="client-info">
                                                <h3>Mr.G.Elango</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Single Testimonial style2 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Testimonial Style2 Area-->

        <!--Start latest blog area style2-->
        <section class="latest-blog-area style2">
            <div class="container inner-content">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="sec-title float-left">
                            <p>News & Updates</p>
                            <div class="title">Latest From <span>Blog</span></div>
                        </div>
                        <div class="more-blog-button float-right">
                            <a class="btn-two" href="<?php echo base_url();?>blog">More News<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--Start single blog post-->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/siteimg/home/blog-1.jpg" alt="Awesome Image">
                                <div class="overlay-style-two"></div>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="link-icon">
                                            <a href="<?php echo base_url();?>blog"><span class="flaticon-zoom"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <!--<div class="post-date">
                                    <h3>02 <span>Mar 2019</span></h3>
                                </div>
                                <div class="meta-box">
                                    <ul class="meta-info">
                                        <li>By <a href="#">Rubin Santro</a></li>
                                        <li>In <a href="#">Contemporary</a></li>
                                    </ul>
                                </div>-->
                                <h3 class="blog-title"><a href="<?php echo base_url();?>blog">Low cost interior designing ideas</a></h3>
                                <div class="text">
                                    <p>Same as saying through shrinkings from toil & our pain these cases perfectly simple.</p>
                                    <a class="btn-two" href="<?php echo base_url();?>blog">Read More<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single blog post-->
                    <!--Start single blog post-->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-post wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/siteimg/home/blog-2.jpg" alt="Awesome Image">
                                <div class="overlay-style-two"></div>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="link-icon">
                                            <a href="<?php echo base_url();?>blog"><span class="flaticon-zoom"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <!--<div class="post-date">
                                    <h3>24 <span>Feb 2019</span></h3>
                                </div>
                                <div class="meta-box">
                                    <ul class="meta-info">
                                        <li>By <a href="#">Mark Richrdson</a></li>
                                        <li>In <a href="#">Modern Paint</a></li>
                                    </ul>
                                </div>-->
                                <h3 class="blog-title"><a href="<?php echo base_url();?>blog">Commercial design for project</a></h3>
                                <div class="text">
                                    <p>Which is the same as saying through shrinking from toil and pain. These cases are perfectly.</p>
                                    <a class="btn-two" href="<?php echo base_url();?>blog">Read More<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single blog post-->
                    <!--Start single blog post-->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-blog-post wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                            <div class="img-holder">
                                <img src="<?php echo base_url(); ?>themes/siteimg/home/blog-3.jpg" alt="Awesome Image">
                                <div class="overlay-style-two"></div>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="link-icon">
                                            <a href="<?php echo base_url();?>blog"><span class="flaticon-zoom"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-holder">
                                <!--<div class="post-date">
                                    <h3>18 <span>Jan 2019</span></h3>
                                </div>
                                <div class="meta-box">
                                    <ul class="meta-info">
                                        <li>By <a href="#">Rubin Santro</a></li>
                                        <li>In <a href="#">Contemporary</a></li>
                                    </ul>
                                </div>-->
                                <h3 class="blog-title"><a href="<?php echo base_url();?>blog">Our interior design prediction 2019</a></h3>
                                <div class="text">
                                    <p>Every pleasure is to be welcomed every pain avoided. in certain circumstances obligations.</p>
                                    <a class="btn-two" href="<?php echo base_url();?>blog">Read More<span class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single blog post-->

                </div>
            </div>
        </section>
        <!--End latest blog area style2-->

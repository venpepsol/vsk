		<!--Start breadcrumb area-->
      <section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title">
                                <h1>Terms and Conditions</h1>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="<?php echo base_url();?>">Home</a></li>
                                    <li class="active">Terms and Conditions</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->
      <section class="core-features-section">
         <div class="container">
            <?php
               $sitename = $this->config->item('sitename');
               $websiteaddress = $this->config->item('websiteaddress');
                  $mailid = $this->config->item('mailid');
               $country = $this->config->item('country');
                  ?>	
            <div class="container myterms">
               <!-- Main Title -->
               <div class="mt-5 mb-5">
                  <div class="text-justify">
                     <h6 class="head_class mb-3"><?php echo $sitename; ?> Website Terms of Use</h6>
                     <p class="text-justify">The following terms govern the access, viewing or use by you of <a title="Facets<?php echo $websiteaddress; ?>" href="<?php echo $websiteaddress; ?>"><?php echo $websiteaddress; ?></a> (hereinafter referred to as the "Website"). The Website is a beta version of the website, owned and operated by <?php echo $sitename; ?> (hereinafter referred to as the "Company"). These Beta Website Terms of Use (hereinafter referred to as "Terms") are the terms and conditions upon which you have been permitted by the Company of the Website to use this beta version of the Website. By accessing, viewing or using the Website you agree to be bound by these Terms. The Terms may be modified at any time by the Company, and will be reflected on this page. Any modified Terms shall be applicable to your use of the Website without need for any further consent. Your use of this Website including all the features and future developments made to the Website shall be governed by these Terms.</p>
                     <p class="text-justify">Upon completion of the beta stage, any further use of the newly created Website shall be based on the acceptance of the Terms of Use of the final Website released by the Company.</p>
                     <ul class="text-justify">
                        <li>
                           <strong> Acceptance of Terms of Use</strong>
                           <p class="text-justify">By accessing and using the Website you agree and acknowledge that the Company has made this Website available to you without any consideration as it is a beta Website. You further acknowledge that this is a test Website and may not function fully as it otherwise would in its final form and may contain errors. All risk that arises from the use of this Website shall be borne by you, including risks related to loss of data or damage to your computer system.</p>
                        </li>
                        <li>
                           <strong> Website Access and Terms of Use</strong>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">You agree and understand that this Website and the contents are intended for people who are of the age of 18 years and above. You hereby represent that you are 18 years of age or older.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You understand that for the purpose of effectively using the Website you may be required to provide personal information including but not limited to your name, location and contact details (hereinafter referred to as "User Information").</p>
                              </li>
                              <li>
                                 <p class="text-justify">You represent that you have all rights required to provide the information that you have provided to the Website, including the User Information and represent that they are true and accurate.</p>
                              </li>
                              <li>
                                 <p class="text-justify">By uploading User Information on the Website you agree that such information is not proprietary or confidential. You agree that this User Information can be used and viewed by the Company or any third party who has the requisite accesses rights to the same under an agreement with the Company.</p>
                              </li>
                              <li>
                                 <p class="text-justify">For the purpose of registration and use of the Website, you will be sent a username and password at the email address supplied by you. You will be solely responsible for maintaining the confidentiality of the password and agree that the password is intended for use solely by you. You agree and undertake to access the Website only using the username and password sent to you. The Company may in its sole discretion permit or deny your registration.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You will be solely responsible for your acts on the Website including those conducted under the username assigned to you.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You understand and agree that the Company may in its sole discretion terminate your access to the Website without notice and you waive, any right to claim access to the Website or any other claim that you may have. Any data of your usage may be retained or deleted at the Company's sole discretion.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You agree that you will use the Website only in relation to seeking services or goods <?php echo $country;?>, and shall not share information from the Website with any third party who is not a permitted user of the beta Website.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You acknowledge and understand that the listings provided on the Website are a directory service and not an endorsement by the Company of the vendor's capabilities or services.</p>
                              </li>
                              <li>
                                 <p class="text-justify">The Website may contain links to third party websites either in the contents or the advertisements. The Company is not responsible in any manner for the contents of these third party websites and if you should choose to use the link to view the third party websites, you choose to do so at your own risk. The Company does not endorse the contents nor in any manner represent the accuracy or correctness of information on such third party websites.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Permitted Uses</strong>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">You agree that you will use the Website only for viewing vendor postings, uploading vendor information and contacting prospective vendors, as applicable, in accordance with the Terms.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You will use the Website and any information available in accordance with all applicable laws and regulations.</p>
                              </li>
                              <li>
                                 <p class="text-justify">You agree that you will use the video messaging services only to advertise services if you are a vendor, or to create personalized invitations or responses to invites. You understand that sending false or offensive messages through communication services is an offence punishable under law.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Prohibited Use</strong>
                           <p class="text-justify">In relation to the use and access of the Website its content and features, you agree and undertake not to:</p>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">upload, transmit or publish any information on behalf of a third party, including any user Information of any person other than you, more specifically, you will not impersonate another person;</p>
                              </li>
                              <li>
                                 <p class="text-justify">upload, transmit or publish any information or material which, is threatening, abusive, obscene, derogatory (in any form), defamatory or libelous, caste discriminatory, racially or ethnically objectionable or contains pornography;</p>
                              </li>
                              <li>
                                 <p class="text-justify">violate the privacy of any person;</p>
                              </li>
                              <li>
                                 <p class="text-justify">upload, transmit or publish any viruses or other malware, to corrupt, interrupt, limit, destroy or otherwise impact the Website, the Company's computer systems, or the computer systems of other users or third party systems;</p>
                              </li>
                              <li>
                                 <p class="text-justify">upload transmit or publish anything which you do not have the rights to or any material which infringes the intellectual property rights (in whatever form) of any third party;</p>
                              </li>
                              <li>
                                 <p class="text-justify">use the Website in any manner which is not permitted under these Terms or in any manner which is illegal or unethical;</p>
                              </li>
                              <li>
                                 <p class="text-justify">access the Website in any unauthorized manner, including by hacking or using the log in credentials of any other user;</p>
                              </li>
                              <li>
                                 <p class="text-justify">use the Website for any marketing purposes or for sending any advertisements or unsolicited materials.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Privacy and Personally Identifiable Data</strong>
                           <p class="text-justify">The information collected by the Company on the Website shall be governed by these Terms and the Privacy Policy. All information collected shall be available to you to modify as required. Sensitive personal information shall be treated in accordance to applicable Indian laws. Access to the same shall be provided to third parties who have access rights under an agreement with the Company under similar terms. All User Information will be used and processed in accordance with these Terms and the Privacy Policy, you agree that you have read and understood the Privacy Policy.</p>
                        </li>
                        <li>
                           <strong> Intellectual Property</strong>
                           <p class="text-justify">You agree and understand that:</p>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">the contents of the Website including but not limited to the information, logos, designs, databases, arrangement of databases, user interfaces, software, audio, pictures, logos, icons, are the sole property of the Company or their licensors. All intellectual property in and to the Website and its contents and functionalities shall vest solely with the Company.</p>
                              </li>
                              <li>
                                 <p class="text-justify">save for the limited right to access and use the Website in accordance with the Terms, on a non-exclusive and non-transferable basis, there are no other rights being granted to you in the Website or any of the contents and functionalities.</p>
                              </li>
                              <li>
                                 <p class="text-justify">you have no right to make any copies of the whole or part of this Website or any of the contents therein.</p>
                              </li>
                              <li>
                                 <p class="text-justify">you have no rights to remove, modify (including removing any copyright notices or proprietary markings) any part of the Website.</p>
                              </li>
                              <li>
                                 <p class="text-justify">you have no right to use any search mechanisms other than that provided on the Website and will not use any web-crawler or any data harvesting tools to harvest data of any sort from the Website</p>
                              </li>
                              <li>
                                 <p class="text-justify">all feedback provided by you in relation to the use and functionality of the Website can be used by the Company and all rights in the same shall vest with the Company.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Limitation of Liability</strong>
                           <p class="text-justify">The aggregate liability of the Company to you, whether in contract, tort, negligence or otherwise, howsoever arising, whether in connection with these Terms, or your access and use of this Website and its contents and functionalities or for any reason related to the operation of the Website, shall not exceed Rupees hundred (100). In no event shall the Company be liable for any loss of profits (anticipated or real), loss of business, loss of reputation, loss of data, loss of goodwill, loss of employment, any business interruption or any direct, indirect, special, incidental, consequential, punitive, tort or other damages, however caused, whether or not it has been advised of the possibility of such damages.</p>
                        </li>
                        <li>
                           <strong> Indemnity</strong>
                           <p class="text-justify">You agree to indemnify and hold harmless, the Company, its employees and agents, against any claims, losses, damages or costs arising from their</p>
                           <ol type="i">
                              <li>
                                 <p class="text-justify">use of the Website and its contents,</p>
                              </li>
                              <li>
                                 <p class="text-justify">breach of these Terms or</p>
                              </li>
                              <li>
                                 <p class="text-justify">any acts or omission of yours in relation to the Website and its use thereof, including but not limited to postings.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Disclaimer</strong>
                           <p class="text-justify">The Company has exercised reasonable diligence while collecting the data available on the Website, however the Company does not warrant the completeness accuracy of data available on this Website nor does it warrant that the Website will function in accordance to the specifications and documentation as this is a beta Website. Should you procure the services of the vendors listed on the Website you do so at your own risk and liability, the Company shall have no liability or responsibility towards the same.
                              The contents and services on the Website are available on an "as is" basis.
                           </p>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">The Company does not represent in any manner that:</p>
                                 <ol type="i">
                                    <li>
                                       <p class="text-justify">the information, data or contents of the Website are accurate;</p>
                                    </li>
                                    <li>
                                       <p class="text-justify">the Website will be available at all times and will operate error free or that there will be uninterrupted access and service;</p>
                                    </li>
                                    <li>
                                       <p class="text-justify">the integrity of the information on the Website or information you upload will be maintained;</p>
                                    </li>
                                    <li>
                                       <p class="text-justify">it endorses any of the views of any of the users who may have posted contents;</p>
                                    </li>
                                    <li>
                                       <p class="text-justify">it has screened or verified any of the information posted herein;</p>
                                    </li>
                                    <li>
                                       <p class="text-justify">the Website or any content is free from viruses or other malware.</p>
                                    </li>
                                 </ol>
                              </li>
                              <li>
                                 <p class="text-justify">Subject to applicable laws, the Company hereby disclaims all warranties, whether express or implied, in relation to the Website, its contents and functionalities, including but not limited to warranties of merchantability or fitness for a particular purpose. Where applicable laws do not permit the disclaimer of warranties to the extent detailed above, the said warranties are disclaimed to the maximum limit permitted by applicable law.</p>
                              </li>
                              <li>
                                 <p class="text-justify">The Company reserves the right to remove any contents on the Website in its sole discretion.</p>
                              </li>
                           </ol>
                        </li>
                        <li>
                           <strong> Complaints</strong>
                           <p class="text-justify">Should you have any complaints regarding the Website, including but not limited to abuse and misuse of the Website, copyright infringement and the like, or any issues related to data privacy please report the same to: <a title="Mail US" href="mailto:<?php echo $mailid; ?>"><?php echo $mailid; ?></a>
                              Please provide your name, email address, physical address and contact numbers so that the Company may be in a position to verify details or check the authenticity of the complaints.
                           </p>
                        </li>
                        <li>
                           <strong> Links to third Party Sites</strong>
                           <p class="text-justify">You shall not create a link to a page of this website without our prior permission. If you do so, such creation shall be at your risk and the Company shall only be liable to the extent set forth in these Terms.</p>
                        </li>
                        <li>
                           <strong> Miscellaneous Provisions</strong>
                           <ol type="a">
                              <li>
                                 <p class="text-justify">Termination: The Company reserves the rights to terminate access to the Website at any time, in its sole discretion. You acknowledge the Company's right to do and waive any claim that arises from such termination. Provisions which by their nature are intended to survive, shall survive termination and continue to be applicable.</p>
                              </li>
                              <li>
                                 <p class="text-justify">Force Majeure: in no event shall the Company be liable for any acts beyond its control or for any acts of god.</p>
                              </li>
                              <li>
                                 <p class="text-justify">Access: The Company does not make any claim that the Website and its contents may be lawfully viewed or accessed outside of India. You are solely responsible for complying with all applicable laws.</p>
                              </li>
                              <li>
                                 <p class="text-justify">Waiver: No waiver of any provision of these Terms of Use shall be binding unless executed and notified by the Company.</p>
                              </li>
                              <li>
                                 <p class="text-justify">Severability: If any provisions of these Terms are determined to be invalid or unenforceable, it will not affect the validity or enforceability of the other provisions of these Terms, which shall remain in full force and effect.</p>
                              </li>
                              <li>
                                 <p class="text-justify">Governing Law: These Terms are governed by the laws of the Republic of India. The courts at Coimbatore, India shall have exclusive jurisdiction over any claims or matters arising out of these Terms or in relation to any matter related to the Website.</p>
                              </li>
                           </ol>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="clear"></div>
            </div>
            <!-- End Content -->
         </div>
      </section>
		<!--Start breadcrumb area-->
        <section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title">
                                <h1>About Us</h1>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li class="active">About</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->

        <!--Start about area-->
        <section class="about-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5">
                        <div class="about-image-box">
                            <div class="inner-box">
                                <img src="<?php echo base_url(); ?>themes/siteimg/about-img.jpg" alt="Awesome Image">
                                <div class="overlay">
                                    <div class="box">
                                        <div class="icon wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms">
                                            <img src="<?php echo base_url(); ?>themes/siteimg/about-em.png" alt="Home Icon">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-box">
                                <p>25+ Years leading the way in building and civil construction</p>
                                <h3>Mr. K.Peachiappan, <span>Founder</span></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="about-text">
                            <div class="sec-title">
                                <p>About Company</p>
                                <div class="title">REDEFINING <span>LIFESTYLE</span> OF PEOPLE</div>
                            </div>
                            <div class="inner-content">
                                <div class="text">
                                    <p align="justify">The genesis of VSK Housing India  brand goes back to 90s when first generation entrepreneur Mr.K.Peachiappan founded VSK Housing India. He laid the foundation for VSK Interiors and was one of the leading interiors in the city. In 2014, the company made in roads into the evolving construction sector. Within a short span of time, the company emerged as one of the largest construction companies marked by some of the highly appreciating projects achieving accolades and more importantly the trust of the people.</p>
                                    <p align="justify">Today VSK Housing India stands tall on the foundation of its values – values of delivering quality, ensuring client satisfaction and redefining lifestyle of people. Quality systems and on time delivery make our clients rely on us with all trust. Talented team of architects, structural engineers and designers are our strength.</p>
                                </div>
                                <div class="about-carousel-box owl-carousel owl-theme">
                                    <!--Start Single Box-->
                                    <div class="single-box">
                                        <div class="icon-holder">
                                            <img src="<?php echo base_url(); ?>themes/siteimg/icon/target.png"/>
                                        </div>
                                        <div class="text-holder">
                                            <h3>Our Mission</h3>
                                            <p>Same as saying through shrinking from pain these perfectly simple and easy to distinguish.</p>
                                        </div>
                                    </div>
                                    <!--End Single Box-->
                                    <!--Start Single Box-->
                                    <div class="single-box">
                                        <div class="icon-holder">
                                            <img src="<?php echo base_url(); ?>themes/siteimg/icon/vision.png"/>
                                        </div>
                                        <div class="text-holder">
                                            <h3>Our Vision</h3>
                                            <p>Same as saying through shrinking from pain these perfectly simple and easy to distinguish.</p>
                                        </div>
                                    </div>
                                    <!--End Single Box-->
                                    <!--Start Single Box-->
                                    <div class="single-box">
                                        <div class="icon-holder">
                                            <img src="<?php echo base_url(); ?>themes/siteimg/icon/quality.png"/>
                                        </div>
                                        <div class="text-holder">
                                            <h3>Our Values</h3>
                                            <p>Same as saying through shrinking from pain these perfectly simple and easy to distinguish.</p>
                                        </div>
                                    </div>
                                    <!--End Single Box-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--End about Area-->
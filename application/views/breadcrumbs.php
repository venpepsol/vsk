<section id="subheader">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h1>
                <?php echo $title; ?>
              </h1>
            </div>
            
            <!-- devider -->
            <div class="col-md-12">
              <div class="devider-page">
                <div class="devider-img-right">
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <ul class="subdetail">
                <li>
                  <a href="<?php echo base_url(); ?>">Home</a>
                </li>

                <li class="sep">/
                </li>

                <li><?php echo $title; ?>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
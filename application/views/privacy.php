		<!--Start breadcrumb area-->
        <section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content clearfix">
                            <div class="title">
                                <h1>Privacy Policy</h1>
                            </div>
                            <div class="breadcrumb-menu float-right">
                                <ul class="clearfix">
                                    <li><a href="<?php echo base_url();?>">Home</a></li>
                                    <li class="active">Privacy Policy</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->
		<section class="core-features-section">
			<div class="wrapsemibox mt-5 mb-5">
				<?php
					$sitename = $this->config->item('sitename');
					$websiteaddress = $this->config->item('websiteaddress');
					$mailid = $this->config->item('mailid');
					$country = $this->config->item('country');
				?>				
				<!-- Content -->
				<div class="container">
					<!-- Main Title --> 
					<div class="text-justify">
						<h6 class="head_class">Introduction:</h6>
						<p><?php echo $sitename; ?> recognizes the importance of your privacy and is committed to ensuring that you are aware of how your information is being used. This policy describes the types of information the Company may collect from you or that you may provide in relation to the use or access of the Website, </p>
						<p><a title="<?php echo $websiteaddress; ?>" href="<?php echo $websiteaddress; ?>"><?php echo $websiteaddress; ?></a> 
						and the manner in which such information is collected used, processed, disclosed and maintained. Capitalised terms not defined herein shall have the same meaning as those ascribed to them in the Terms of Use. Please read and understand the policy carefully. If you do not agree with our policies and our practices in the way we treat User Information, your choice is to not use the Website. Your use or access of the Website, shall constitute your agreement to this Privacy Policy. By accepting the Privacy Policy and the Terms of use, you expressly consent to the Company's use and disclosure of your personal information in accordance with this Privacy Policy.</p>
						<h6 class="head_class">Applicability of the policy:</h6>
						<p>This policy shall apply to all information the Company collects on the Website, including but not limited to any information you upload, emails that you exchange with the Company, any information submitted by you to the Company. The policy does not apply to, nor does the Company take any responsibility for, any information that is collected by any third party either using the Website or through any links on the Website or through any of the advertisements.</p>
						<h6 class="head_class">Nature Of Information Collected And Manner Of Collection :</h6>
						<p>In the use of the Website, as you navigate through the Website, the Company may collect different types of information. This may include User Information, information that is personally identifiable, other information which may not be personally identifiable, information on the usage patterns of any user including you, searches that you have done on the Website, advertisements or third party links that you have visited, any emails or other correspondence you have exchanged on the Website or with the Company. You understand that the information collected by the Company, may be collected directly or through tracking of your usage of the Website. The usage details may include IP addresses, details of your computer equipment, browser, location, connections, any information that the Company may collect through the use of cookies and other tracking technologies. The collection of data may in most cases be automatic. "Cookies" are files that would be placed in your system's hard drive and are intended to improve your user experience, by enabling the Company to track your usage and preference. These cookies may track your Website usage, advertisement and links that you visit and other User Information. Most cookies the Company uses are limited to a session, which mean they will be automatically deleted when your session closes. You may decline these cookies by changing your browser settings, if permitted, however do note that this may impact certain features of the Website or your user experience in using the Website.</p>
						<p>Note: Advertisements on the Website may be posted by third-party advertisers, and such third parties may use cookies and other automatic tracking technologies to collect information about you, including but not limited to web behavioural information and patterns. The Company does not control nor takes any responsibility for such third parties, their collection and use of information or their tracking technologies or how they may be used. You also may provide information to be published or displayed or posted on the Website, or transmitted to other users of the Website or third parties. Any such information is posted or transmitted to others at your own risk. Please be aware that the Company cannot control the actions of other users of the Website with whom you may choose to share information with.</p>
						<h6 class="head_class">Use of Your Information:</h6>
						<p>The information that we collect on the Website will be used for the purposes of allowing you to procure, advertise or bid for projects related to the provision of information technology services or information technology enabled services. The information including any User Information will be available to other registered users of the Website. No personally identifiable information will be disclosed or shared with any third party without your express consent. For the purposes of this policy, personally identifiable information shall mean name; age; gender; bank account information; telephone numbers; location data; email addresses; payment, billing or shipping information; customer service information about you as a user of the Website; or other information specifically related to your use of particular services or offerings of the Company, including information that you publicly post using tools made available by the Company. Please do not include any personal information, personally identifiable information or sensitive personal information as part your submissions to the Company, except to the extent specifically needed to register on the Website. If the Company determines that any information you have provided or uploaded violates the terms of this Privacy Policy, the Company has the right, in its absolute discretion, to delete or destroy such information without incurring any liability to you. The Company will not publish, sell or rent your personal information to third parties for their marketing purposes without your explicit consent. The Company may also use the information for analytical purposes, including but not limited to assessing usage data, usage patterns, estimate audience sizes and other similar activities. You agree that we may use your personal information to contact you and deliver information or targeted advertisements, administrative notices and any other communication relevant to your use of the Website.</p>
						<h6 class="head_class">Disclosure of Your information:</h6>
						<p>The User Information you provide on the Website may be disclosed by the Company to its subscribers and registered users, other third party's who access the Website, third party advertisers, our subsidiaries and affiliates, or to other third party service providers of the Company who require the information for the purposes of operating and maintaining the Website. The Company will comply with requests and directions of all governmental, law enforcement or regulatory authorities, which it believes in good faith to be in accordance with any applicable law. Such compliance may include providing User Information, personally identifiable information or any other information available to such agency or authority. By providing any information on the Website, you consent to us providing such information to any governmental, law enforcement or regulatory authorities who exercise jurisdiction over the Company and the Website.</p>
						<h6 class="head_class">Data Security:</h6>
						<p>The information that you provide, subject to disclosure in accordance with this Privacy Policy and Terms of Use, shall be maintained in a safe and secure manner. The Company's databases and information are stored on secure servers with appropriate firewalls. As a user of the Website, you have the responsibility to ensure data security. You should use the Website in a responsible manner. Do not share your username or password with any person. You are solely responsible for all acts done under the username you are registered under. Given the nature of internet transactions, the Company does not take any responsibility for the transmission of information including User Information to the Website. Any transmission of User Information on the internet is done at your risk. The Company does not take any responsibility for you or any third party circumventing the privacy settings or security measures contained on the Website. While the Company will use all reasonable efforts to ensure that your User Information and other information submitted by you is safe and secure, it offers no representation, warranties or other assurances that the security measures are adequate, safe, fool proof or impenetrable.</p>
						<h6 class="head_class">Accessing and Updating Your Information:</h6>
						<p>You can change, alter of otherwise modify or update your User Information at any time by accessing the Website using your registered username and accessing your user account. You may also change and/or delete any of the information you have submitted. Do note however, the Company reserves the rights to save any usage information and you are not entitled to seek the deletion of the same. The Company at its sole discretion may permit or deny the change of any information, if it is believes the same is required to observe applicable laws.</p>
						<h6 class="head_class">Age restrictions:</h6>
						<p>The Website is only intended for users who are of 18 years of age or older. If you are not of the requisite age you are not to provide any User Information or other information. If it comes to the Company's attention that any User Information or information pertains to an individual under the age of 18 years, such User Information or information will be deleted without notice to you.</p>
						<h6 class="head_class">Amendments to the Privacy Policy:</h6>
						<p>This Privacy Policy is subject to change at the Company's sole discretion. Any changes to the Privacy Policy will be notified by a notice on the home page.</p>
						<h6 class="head_class">Terms of Use:</h6>
						<p>This Privacy policy shall form a part of the Terms of Use
						Third party vendors, including Google, use cookies to serve ads based on a user's prior visits to your website.
						Google's use of the DoubleClick cookie enables it and its partners to serve ads to your users based on their visit to your sites and/or other sites on the Internet. Users may opt out of the use of the DoubleClick cookie for interest-based advertising by visiting Ads Settings. (Alternatively, you can direct users to opt out of a third-party vendor's use of cookies for interest based advertising by visiting aboutads.info.)</p>
					</div>        
				</div>
				<!-- End Content -->
			</div>
		</section>
	
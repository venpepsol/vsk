<section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                        <h1>FAQ's</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class="active">FAQ's</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="faq-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="faq-content-box">
                    <div class="accordion-box">
                        <!--Start single accordion box-->
                        <div class="accordion accordion-block">
                            <div class="accord-btn active">
                                <h4>Coimbatore as a living and investment destination?</h4>
                            </div>
                            <div class="accord-content collapsed pl-4">
                                <p>*Yes, Coimbatore has already become the destination for Education, Health, Business, Industries, Religeous places, Shopping Malls etc. The mild and pleasant weather (major part of the year) makes it a loveable city. Good road and rail connections with other cities and both national and international air connectivity makes it really travel friendly.</p>
                            </div>
                        </div>
                        <!--End single accordion box-->
                        <!--Start single accordion box-->
                        <div class="accordion accordion-block">
                            <div class="accord-btn">
                                <h4>4 questions to ask when choosing a construction company ?</h4>
                            </div>
                            <div class="accord-content pl-4">
                                <p>
                                    <ul>                                 
                                        <li>1. How many projects have been completed till now and how many projects under way?</li>
                                        <li>2. Do you have professional monitoring of the construction? Yes, apart from professional employees our our own Directors are qualified construction professionals.</li>
                                        <li>3. Will the documents be clear and without any encumbrance? Yes, of course, it will be.</li>
                                        <li>4. Will you ensure usage of quality materials? Yes, because as per our own internal quality systems, only tested and approved materials can be received in our construction sites.</li>
                                        <li>5. Any power back up for individual flats also? Actually, along with power for common areas, individual flat also will have power back up upto 500 Watts covering basic needs.</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <!--End single accordion box-->
                        <!--Start single accordion box-->
                        <div class="accordion accordion-block">
                            <div class="accord-btn">
                                <h4>10 things you spouse would love in a new home?</h4>
                            </div>
                            <div class="accord-content pl-4">
                                <p>
                                    <ul>                                 
                                        <li>1. Convenient kitchen with easily accessible almirahs / cup boards.</li>
                                        <li>2. Bigger wash sink for vessel cleaning,</li>
                                        <li>3. Window or an exhaust fan,</li>
                                        <li>4. Provision for Chimney,</li>
                                        <li>5. Decorative tiles in the kitchen,</li>
                                        <li>6. Anti skid tiles in the bath rooms,</li>
                                        <li>7. Convenient Power points for Mixies, Fridge, Microwave oven, grinders, Washing machine etc,</li>
                                        <li>8. Convenience for drying the clothes,</li>
                                        <li>9. Cup boards for storing the cloth materials,</li>
                                        <li>10. Lofts for storage of other materials.</li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                        <!--End single accordion box-->
                        <!--Start single accordion box-->
                        <div class="accordion accordion-block">
                            <div class="accord-btn">
                                <h4>Location advantages of VSK's ASHWATTHA?</h4>
                            </div>
                            <div class="accord-content pl-4">
                                <p>Located @ heart of Coimbatore (still away from noise) Nanjundapuram Road, Ramanathapuram.</p>
                                <p>Close (20 mts) to top Schools, Colleges, Hospitals, Malls, Railway Station, religeous places and Restaurants for general needs*.</p>
                            </div>
                        </div>
                        <!--End single accordion box-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
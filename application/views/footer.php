<!--Start slogan area-->
        <section class="slogan-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content flex-box-two fix">
                            <div class="title float-left">
                                <h3>Quality systems and on time delivery make our clients rely on us with all trust.</h3>
                            </div>
                            <div class="button float-right">
                                <a class="btn-one" href="javascript:void();" type="button" data-toggle="modal" data-target="#exampleModalCenter">Enquire Now<span class="flaticon-next"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End slogan area-->
        <!--Start footer area Style4-->
        <footer class="footer-area style4">
            <div class="container">
                <div class="row">
                    <!--Start single footer widget-->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div class="single-footer-widget marbtm50-s4">
                            <div class="our-info-box">
                                <div class="footer-logo">
                                    <a href="<?php echo base_url();?>">
                                        <img src="<?php echo base_url(); ?>themes/siteimg/logow.png" alt="Awesome Logo">
                                    </a>
                                </div>
                                <div class="text">
                                    <p align="justify">VSK Housing India is one of the leading real estate development companies. having footprints in Coimbatore for last 25 years. The company has already delivered more than 2,50,000 sq. ft.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single footer widget-->
                    <!--Start single footer widget-->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        <div class="single-footer-widget s4">
                            <div class="title-style2">
                                <h3>Quick Links</h3>
                            </div>
                            <div class="usefull-links">
                                <ul class="float-left">
                                    <li><a href="<?php echo base_url();?>about">About</a></li>
                                    <li><a href="<?php echo base_url();?>project">Projects</a></li>
                                    <li><a href="<?php echo base_url();?>blog">Blog</a></li>
                                    <li><a href="<?php echo base_url();?>faq">FAQ's</a></li>
                                    <li><a href="<?php echo base_url();?>contact">Contact</a></li>
                                </ul>
                                <ul class="float-left borders-left">
                                    <li><a href="<?php echo base_url();?>ongoing">Ongoing Projects</a></li>
                                    <li><a href="<?php echo base_url();?>upcoming">Upcoming Projects</a></li>
                                    <li><a href="<?php echo base_url();?>completed">Completed Projects</a></li>
                                    <li><a href="<?php echo base_url();?>privacypolicy">Privacy Policy</a></li>
                                    <li><a href="<?php echo base_url();?>termsofuse">Terms of Use</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--End single footer widget-->
                    <!--Start single footer widget-->
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                        <div class="single-footer-widget footcontact pdtop50-s3">
                            <div class="title-style2">
                                <h3>Contact</h3>
                            </div>
                                <ul>
                                    <li>
                                        <i class="fa fa-map-marker footicon"></i><p>1/11, First Floor, K.S.Complex, Thadagam Rd, <br>TVS Nagar, Coimbatore - 641025</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone footicon"></i><p><a href="tel:+914222403388">+91 422 2403388</a>, <a href="tel:+919585611222">+91 95856 11222</a>,<br><a href="tel:+91 78670 22256">+91 78670 22256</a></p>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope footicon"></i><p><a href="mailto:gmcbe@vskhousingindia.com">gmcbe@vskhousingindia.com</a></p>
                                    </li>
                                </ul>
                        </div>
                    </div>
                    <!--End single footer widget-->
                </div>
            </div>
        </footer>
        <!--End footer area style4-->
        <!--Start footer bottom area-->
        <section class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="footer-bottom-content flex-box-two">
                            <div class="copyright-text">
                                <p>© <script type="text/javascript">document.write(new Date().getFullYear());</script> All Rights Reserved by VSK Housing India | Designed by <a href="https://venpep.com/" target="_blank">VenPep</a></p>
                            </div>
                            <div class="footer-social-links float-right">
                                <span>We are On:</span>
                                <ul class="sociallinks-style-one">
                                    <li class="wow slideInUp" data-wow-delay="0ms" data-wow-duration="1200ms">
                                        <a href="https://www.facebook.com/VSKHOUSINGINDIA" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="wow slideInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                                        <a href="https://www.instagram.com/vsk_housing_india/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End footer bottom area-->
        <!-- enquiry popup -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title w-100 text-center" id="exampleModalLongTitle">Quick Enquiry</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body contact-form">
                        <div class="p-3">
                            <form id="contact-form" name="contact_form" class="default-form" action="" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-box">
                                            <input type="text" name="form_name" value="" placeholder="Name" required="">
                                            <div class="icon">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-box">
                                            <input type="email" name="form_email" value="" placeholder="Email Address" required="">
                                            <div class="icon">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-box">
                                            <input type="text" name="form_subject" value="" placeholder="Subject">
                                            <div class="icon">
                                                <i class="fa fa-file" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-box">
                                            <input type="text" name="form_phone" value="" placeholder="Phone">
                                            <div class="icon">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-box">
                                            <textarea name="form_message" placeholder="Your Message..." required=""></textarea>
                                            <div class="icon envelop">
                                                <i class="fa fa-comment" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="button-box">
                                            <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                            <button class="btn-one" type="submit" data-loading-text="Please wait...">Submit<span class="flaticon-next"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
    <script src="<?php echo base_url(); ?>themes/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/appear.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/bootstrap-select.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/isotope.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.bootstrap-touchspin.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.countTo.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.enllax.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.fancybox.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.mixitup.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/jquery.paroller.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/owl.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/validation.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/wow.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/map-helper.js"></script>
    <script src="<?php echo base_url(); ?>themes/assets/language-switcher/jquery.polyglot.language.switcher.js"></script>
    <script src="<?php echo base_url(); ?>themes/assets/timepicker/timePicker.js"></script>
    <script src="<?php echo base_url(); ?>themes/assets/html5lightbox/html5lightbox.js"></script>
    <!--Revolution Slider-->
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="<?php echo base_url(); ?>themes/js/main-slider-script.js"></script>
    <!-- thm custom script -->
    <script src="<?php echo base_url(); ?>themes/js/custom.js"></script>
    <script>
        $(document).ready(function(){
            $('.main-menu .navbar-collapse ul li a').click(function(){
                $('li a').removeClass("active");
                $(this).addClass("active");
            });
        });
    </script>
    
</body>
</html>
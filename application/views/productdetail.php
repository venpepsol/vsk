<!--page title start-->
<style>
	.btn.btn-iconic{
		padding: 12px 20px 12px 20px;
	}
	.product-btn{
		padding: 17px 5px;
    text-align: center;
    font-size: 18px;
    color: #fff;
    background: #f05125;
	}
	.product-btn a{
		color: #fff;
	}
	.product-img{
		height:260px;
	}
	iframe{
	pointer-events: auto;
}
.slick-slide .img-fluid{
	height: auto !important;
	width: 90% !important;
}
</style>

<!--page title end-->


<!--body content start-->

<div class="page-content">
 <?php
              $minorder = '';
              $packaging = '';
              $packDes = '';
              if($specifi != '')
              {
			  	foreach(json_decode($specifi) as $row)
			  	{ 
	              if($row->speckey == 'Min. Qty')
	              {
	              	$minorder = $row->specvalue;
				  }
	              if($row->speckey == 'Packaging')
	              {
	              	$packaging = $row->specvalue;
				  }
				}
			  }
               
			  $minDes = $catName.' '.$proTitle.' is available with us in minimum order quantity of '.$minorder;
			  if($packaging != '')
			  {
			  	$packDes = ' and comes with '.$packaging.' packaging.';
			  }

			  $proDesc = $minDes.$packDes." Our range of products are unique and trendy. Check out for more products from the available categories to get a glance of our collections. For more classic customized products get in touch with us now."
              ?>
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-md-12">
        <div class="slick3">
       
			<div align="center">
            <img class="img-fluid w-100" src="<?php echo base_url().$imagepath; ?>" alt="">
          </div>
       <!--   <div class="item-slick3" data-thumb="<?php echo base_url(); ?>themes/images/product-thumb/01.jpg">
            <img class="img-fluid w-100" src="<?php echo base_url(); ?>themes/images/product/01.jpg" alt="">
          </div>
          <div class="item-slick3" data-thumb="<?php echo base_url(); ?>themes/images/product-thumb/02.jpg">
            <img class="img-fluid w-100" src="<?php echo base_url(); ?>themes/images/product/02.jpg" alt="">
          </div>
          <div class="item-slick3" data-thumb="<?php echo base_url(); ?>themes/images/product-thumb/03.jpg">
            <img class="img-fluid w-100" src="<?php echo base_url(); ?>themes/images/product/03.jpg" alt="">
          </div>
          <div class="item-slick3" data-thumb="<?php echo base_url(); ?>themes/images/product-thumb/04.jpg">
            <img class="img-fluid w-100" src="<?php echo base_url(); ?>themes/images/product/04.jpg" alt="">
          </div>-->
        </div>
        <!--<div class="slick3-dots-main"></div>-->
      </div>
      <div class="col-lg-7 col-md-12 md-mt-5">
      <div class="product-details">
        <h4>
        <?php echo $proTitle; ?>
        </h4>
       <!-- <div class="product-price my-4"> <span class="mr-3"> $179.99 <del>$279.00</del></span>
          
        </div>-->
        <ul class="portfolio-meta list-unstyled mb-4">
          <li class="mb-1"><span class="orangecolor"> PRODUCT CODE: </span> <?php echo $proCode; ?></li>
          <li class="mb-1"><span class="orangecolor"> CATEGORIES :</span> <?php echo $catName;if($subCatName != ''){ echo ', '.$subCatName;} ?></li>
         <!-- <li class="mb-1"><span class="orangecolor"> DIMENSION (LXBXH) MM :</span> 55 x 40 x 55</li>-->
          <li class="mb-1"><span class="orangecolor"> DESCRIPTION :</span> <?php echo $proDesc; ?></li>
        </ul>
        
         
                 
        <button class="btn btn-theme btn-iconic mt-3" data-toggle="modal" data-target="#promodel">Enquire Now</button>
      </div>
    </div>
  </div>
  </div>
</section>


<!--tab start-->

<section class="grey-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="tab">
          <!-- Nav tabs -->
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist"> 
              <a class="nav-item nav-link active" id="nav-tab2" data-toggle="tab" href="#tab3-2" role="tab" aria-selected="false">Additional information</a>
              <a class="nav-item nav-link " id="nav-tab1" data-toggle="tab" href="#tab3-1" role="tab" aria-selected="true">Description</a>
            </div>
          </nav>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade  " id="tab3-1">
              <h5 class="mb-3">Product <span class="text-theme">Description</span></h5>
              <!--<p class="lead mb-0"><?php echo $proTitle; ?>, We manufacturing best quality of prodcuts and also contain a variety of other stationery items. We offer a wide range of stationary items which are manufactured using optimum quality raw material to ensure excellent product performance.</p>-->
              <p><?php echo $proDesc; ?></p>
             
              <p></p>
            </div>
            <div role="tabpanel" class="tab-pane fade show active" id="tab3-2">
              <h5 class="mb-3">Additional <span class="text-theme">information</span></h5>
              <table class="table table-striped table-bordered mb-0">
                <tbody>
                  <tr>
                    <td>Product Code:</td>
                    <td><?php echo $proCode; ?></td>
                  </tr>
                  <tr>
                    <td>Categories</td>
                    <td><?php echo $catName;if($subCatName != ''){ echo ', '.$subCatName;} ?></td>
                  </tr>
                  <?php 
                  if($specifi != '')
             	 {
                  foreach(json_decode($specifi) as $row){ ?>
                  <tr>
                    <td> <?php echo $row->speckey; ?></td>
                    <td><?php echo $row->specvalue; ?></td>
                  </tr>
                  <?php } } ?>
                  <!--<tr>
                    <td>Min. Qty </td>
                    <td>15 Pieces</td>
                  </tr>-->
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--tab end-->


<!--product start-->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mr-auto">
        <div class="section-title">
          <h2 class="title">Related <span>Products</span></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="owl-carousel owl-theme no-pb slide-arrow-2" data-dots="false" data-nav="true" data-items="4" data-lg-items="3" data-md-items="2" data-sm-items="2" data-margin="30" data-autoplay="true">
        <?php
        
        $relatedproduct = $this->webmodel->getProduct('',$subCatSlug,$catSlug);
         foreach($relatedproduct as $row){ 
          ?>
          <div class="item">
            <div class="product-item">
              <div class="product-img">
                <img class="img-fluid" src="<?php echo base_url().$row->imagepath; ?>" alt="">
              </div>
              <div class="product-desc"> <a href="<?php echo base_url().'product/'.$row->product_slug; ?>" class="product-name">
                  <?php echo $row->product_name; ?>
                </a>
                <span class="product-price">
                  <?php echo $row->product_code; ?>
                </span> 
              </div>
              <div class="product-btn">
                <a href="<?php echo base_url().'product/'.$row->product_slug; ?>" class="product-name"><?php echo $row->product_name; ?></a>
              </div>
            </div>
          </div>
<?php } ?>
          
        </div>
      </div>
    </div>
  </div>
</section>

<!--product end-->

<!--newsletter start--> 
<section class="theme-bg py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <h2 class="title">For <span>exclusive collections</span>, Get in touch with us now</h2>
      </div>
      <div class="col-lg-12 col-md-12 md-mt-3">
        <div class="subscribe-form">
          <div id="notes"></div>
          <form id="mc-form" class="group row align-items-center">
            <div class="col-sm-6">
            <input type="text" value="" name="enname" class="email" id="mc-name" placeholder="User Name" required="">
            </div>
            <div class="col-sm-6">
            <input type="email" value="" name="email" class="email " id="mc-email" placeholder="Email Address" required="">
            </div>
            <div class="col-sm-6">
            <input type="text" value="" name="mobile" class="email " id="mc-mobile" placeholder="Mobile No" required="">
            </div>
            <div class="col-sm-6">
            <input type="text" value="" name="message" class="email " id="mc-message" placeholder="Message" required="">
            </div>
            <div class="col-sm-4 xs-mt-1">
            <input class="btn btn-white btn-circle" type="submit" id="enquire" value="Enquire Now">
            </div>
            <label for="mc-email" class="subscribe-message"></label>            
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!--newsletter end--> 

</div>
<div id="promodel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><span style="color: #F05125;"><?php echo $proTitle ?></span> Enquiry</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
         <form id="contactform" class="row" method="post">
              <div class="messages" id="contact_results"></div>
              <div class="form-group col-sm-6">
                <input id="username" type="text" name="username" class="form-control" placeholder="User Name" required="required" data-error="Username is required.">
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-sm-6">
                <input id="email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-sm-12">
                <input id="phoneno" type="tel" name="phoneno" class="form-control" placeholder="Phone" required="required" data-error="Phone is required">
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-sm-12">
                <textarea id="message" name="message" class="form-control" placeholder="Message" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                <input type="hidden" name="productname" id="productname" value="<?php echo $proTitle; ?>"/>
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-sm-12" align="center">
                <?php echo $widget; 
                                   echo $script; ?>
              </div>
              <div class="col-sm-12" align="center">
              <button class="btn btn-border btn-radius"><span>Send Message</span>
              </button>
            </div>
            </form>
      </div>
    </div>

  </div>
</div>
<!--body content end--> 
<section class="breadcrumb-area" style="background-image: url(<?php echo base_url(); ?>themes/images/resources/breadcrumb-bg-2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                        <h1>Projects</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li class="active">Projects</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-project-area style4">
    <div class="container">
        <ul class="project-filter post-filter has-dynamic-filters-counter">
            <li data-filter=".filter-item" class="active"><span class="filter-text">All Projects</span></li>
            <li data-filter=".mod"><span class="filter-text">Ongoing Projects</span></li>
            <li data-filter=".contem"><span class="filter-text">Upcoming Projects</span></li>
            <li data-filter=".trad"><span class="filter-text">Completed Projects</span></li>
        </ul>
    </div>
    <div class="container-fluid main-project-style4">
        <div class="row filter-layout masonary-layout justify-content-center">
            <!--Start single project item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 filter-item mod">
                <div class="single-project-style7">
                    <div class="img-holder">
                        <img src="<?php echo base_url(); ?>themes/images/projects/v4-1.jpg" alt="Awesome Image">
                        <div class="overlay-content">
                            <div class="inner-content">
                                <div class="title-box">
                                    <span>Ramanathapuram</span>
                                    <h3><a href="<?php echo base_url(); ?>ashwattha">VSK's Ashwattha Apartment</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span>Ramanathapuram</span>
                        <h3><a href="<?php echo base_url(); ?>ashwattha">VSK's Ashwattha Apartment</a></h3>
                    </div>
                </div>
            </div>
            <!--End single project item-->
            <!--Start single project item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 filter-item contem">
                <div class="single-project-style7">
                    <div class="img-holder">
                        <img src="<?php echo base_url(); ?>themes/images/projects/v4-2.jpg" alt="Awesome Image">
                        <div class="overlay-content">
                            <div class="inner-content">
                                <div class="title-box">
                                    <span>Ramanathapuram</span>
                                    <h3><a href="<?php echo base_url(); ?>aishwaryam">VSK's Aishwaryam Apartment</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span>Ramanathapuram</span>
                        <h3><a href="<?php echo base_url(); ?>aishwaryam">VSK's Aishwaryam Apartment</a></h3>
                    </div>
                </div>
            </div>
            <!--End single project item-->
            <!--Start single project item-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 filter-item trad">
                <div class="single-project-style7">
                    <div class="img-holder">
                        <img src="<?php echo base_url(); ?>themes/images/projects/v4-3.jpg" alt="Awesome Image">
                        <div class="overlay-content">
                            <div class="inner-content">
                                <div class="title-box">
                                    <span>Ramanathapuram</span>
                                    <h3><a href="<?php echo base_url(); ?>aksharam">VSK's Aksharam Apartment</a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span>Ramanathapuram</span>
                        <h3><a href="<?php echo base_url(); ?>aksharam">VSK's Aksharam Apartment</a></h3>
                    </div>
                </div>
            </div>
            <!--End single project item-->
        </div>
    </div>
</section>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminclient extends CI_Controller {
	
public function __construct()
{
	parent::__construct();

	$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
	$this->output->set_header('Pragma: no-cache');
	
	if(($this->session->userdata('userid') == null) || ($this->session->userdata('userid') == ""))
	{
		redirect(base_url().'login');
	}
	
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
}

/*Manju Starts*/

public function client($clientId = '')
{
	$data["clientId"] = $clientId;
	$data["clientName"] = '';
	$data["clientCity"] = '';
	$data["clientState"] = '';
	$data["clientImage"] = '';
	
	$clientDetails = $this->clientmodel->getClientDetails($clientId);
	
	if($clientId > 0)
	{
		if(count($clientDetails) > 0)
		{
			foreach($clientDetails as $row)
			{
				$data["clientName"] = $row->client_name;
				$data["clientCity"] = $row->city;
				$data["clientState"] = $row->state;
				$data["clientImage"] = $row->clientimg;
			}
		}
	}
	else
	{
		$data["clientDetails"] = $clientDetails;
	}
	
	$this->load->view('backend/admin/header');
	$this->load->view('backend/client/clients', $data);
	$this->load->view('backend/admin/footer');
}

public function saveClient()
{
	$clientId = $this->input->post('clientId');
	$clientName = $this->input->post('clientName');
	$clientCity = $this->input->post('clientCity');
	$clientState = $this->input->post('clientState');
	$oldClientImagePath = $this->input->post('oldClientImagePath');
	
	if($clientName != "" && $clientCity != "" && $clientState != "")
	{
		$imgArr = $this->uploadSingleImage('required', 'uploads', 'clients', '', 'clientImage', $oldClientImagePath);
		
		if($imgArr["isError"])
		{
			$data["isError"] = $imgArr["isError"];
			$data["msg"] = $imgArr["msg"];
			echo json_encode($data);
			return;
		}
		$clientImage = $imgArr["imageSrc"];
		
		$this->clientmodel->saveClient($clientId, $clientName, $clientCity, $clientState, $clientImage);
		
		if($clientId > 0)
		{
			$data["isError"] = FALSE;
			$data["msg"] = "Client Details Updated Successfully.";
		}
		else
		{
			$data["isError"] = FALSE;
			$data["msg"] = "Client Details Saved Successfully.";
		}
	}
	else
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill All Details.";
	}
	echo json_encode($data);
}

public function deleteClient()
{
	$clientId = $this->input->post('clientId');
	if($clientId > 0)
	{
		$this->clientmodel->deleteClient($clientId);
		
		$data["isError"] = FALSE;
		$data["msg"] = "Client Removed Successfully.";
	}
	else
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill All Details.";
	}
	echo json_encode($data);
}

public function uploadSingleImage($required = '', $dir1, $dir2, $dir3 = '', $fileName, $oldImgPath = '',$catId='')
{
	$imageSrc = "";
	$imagePath = "";
	
	$dir = './'.$dir1.'/';
	if (!is_dir($dir)) 
	{
	   mkdir($dir);
	}
	
	$dir = './'.$dir1.'/'.$dir2.'/';
	if (!is_dir($dir)) 
	{
	   mkdir($dir);
	}
	
	if($dir3 != "")
	{
		$dir = './'.$dir1.'/'.$dir2.'/'.$dir3.'/';
		if (!is_dir($dir)) 
		{
		   mkdir($dir);
		}
	}
	
	$config['upload_path'] = $dir;
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	$config['max_size']	= '5000';
	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	
	$isError = FALSE;
	$errMsg = "";
	
	if(!$this->upload->do_upload($fileName))
	{
		if($oldImgPath == "")
		{
			if($required == "required" && $catId == "")
			{
				$isError = TRUE;
				$errMsg = strip_tags($this->upload->display_errors());
			}
		}
		$imageSrc = $oldImgPath;
	}
	else
	{
		if($oldImgPath != "")
		unlink($oldImgPath);
		$data = array('upload_data' => $this->upload->data());
		foreach($data as $row)
		{
			$imagePath = $row["raw_name"]."".$row["file_ext"];
		} 
		$imageSrc = $dir.$imagePath;
		$imageSrc = substr($imageSrc, 2);
	}
	
	$resArr = array();
	$resArr["isError"] = $isError;
	$resArr["msg"] = $errMsg;
	$resArr["imageSrc"] = $imageSrc;
	return $resArr;
}

/*Manju Ends*/

}
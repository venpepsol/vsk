<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminproduct extends CI_Controller {
	
public function __construct()
{
	parent::__construct();

	$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
	$this->output->set_header('Pragma: no-cache');
	
	if(($this->session->userdata('userid') == null) || ($this->session->userdata('userid') == ""))
	{
		redirect(base_url().'login');
	}
	if($this->session->userdata('usertype') == 'user')
	{
		redirect(base_url().'user');
	}
	
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
}
public function category($catid='')
{
	$data["catid"] = $catid;
	$data["categorylist"] = $this->productmodel->getcatdetail();
	$categorydetail = $this->productmodel->getcatdetail($catid);
	$data["categoryname"] = '';
	$data["categoryorder"] = '';
	$data["image"] = '';
	$data["categorydesc"] = '';
	$data["categorytype"] = '';
	if($catid > 0)
	{
		foreach($categorydetail as $row)
		{
			$data["categoryname"] = $row->category_name;
			$data["categoryorder"] = $row->order_no;
			$data["categorydesc"] = $row->description;
			$data["categorytype"] = $row->category_type;
			$data["image"] = $row->cat_img;
		}
	}
	$this->load->view("backend/admin/header");
	$this->load->view("backend/product/category",$data);
	$this->load->view("backend/admin/footer");
}
public function deletecategory()
{
	$catid = $this->input->post("catid");
	$res = $this->productmodel->deletecategory($catid);
	$data["iserror"] = FALSE;
	$data["msg"] = 'Category Removed Successfully';
	echo json_encode($data);
	return;
}

public function deletesubcategory()
{
	$catid = $this->input->post("catid");
	$res = $this->productmodel->deletesubcategory($catid);
	$data["iserror"] = FALSE;
	$data["msg"] = 'Sub Category Removed Successfully';
	echo json_encode($data);
	return;
}

public function savecategory()
{
	$catname = $this->input->post("categoryname");
	$categoryorder = $this->input->post("categoryorder");
	$categorydescrption = $this->input->post("desc");
	$categoryType = $this->input->post("categoryType");
	$catid = $this->input->post("catid");
	$slug = '';
	$oldimage = $this->input->post("oldimage");
	$imgpath = $this->uploadSingleImage('required', '/uploads/product', 'productimg', $oldimage);
	if($catname =='')
	{
		$data["iserror"] = true;
		$data["msg"] = "Please Fill Required Fields";
		echo json_encode($data);
		return;
	}
	if($catid =='' || $catid > 0)
	{
		$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $catname);
		$slug = $this->productmodel->checkslug(strtolower($slug),'category','cat_slug');
	}
	$res = $this->productmodel->savecategory($catname,$categorydescrption,$catid,$slug,$imgpath,$categoryType,$categoryorder);
	$data["iserror"] = false;
	if($catid !='')
	{
		$data["msg"] = "Category Updated Successfully";
	}
	$data["msg"] = "Category Created Successfully";
	echo json_encode($data);
	return;
}
public function savesubcategory()
{
	$categoryid = $this->input->post("categoryid");
	$subcategoryname = $this->input->post("subcategoryname");
	$subcatid = $this->input->post("subcatid");
	$slug = '';
	$oldimage = $this->input->post("oldimage");
	$imgpath = $this->uploadSingleImage('required', '/uploads/subcatgory', 'productimg', $oldimage);
	if($subcategoryname =='')
	{
		$data["iserror"] = true;
		$data["msg"] = "Please Fill Required Fields";
		echo json_encode($data);
		return;
	}
	if($subcatid =='' || $subcatid > 0)
	{
		$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $subcategoryname);
		$slug = $this->productmodel->checkslug(strtolower($slug),'category','cat_slug');
	}
	$res = $this->productmodel->savesubcategory($categoryid,$subcategoryname,$subcatid,$slug,$imgpath);
	$data["iserror"] = false;
	if($catid !='')
	{
		$data["msg"] = "Sub Category Updated Successfully";
	}
	$data["msg"] = "Sub Category Created Successfully";
	echo json_encode($data);
	return;
}
public function subcategory($subcatid='')
{
	$data["subcatid"] = $subcatid;
	$data["categorylist"] = $this->productmodel->getcatdetail('','sub_category');
	$data["subcategorylist"] = $this->productmodel->getsubcatdetail();
	$subcategorydetail = $this->productmodel->getsubcatdetail($subcatid);
	$data["subcategoryname"] = '';
	$data["image"] = '';
	if($subcatid > 0)
	{
		foreach($subcategorydetail as $row)
		{
			$data["subcategoryname"] = $row->sub_cat_name;
			$data["image"] = $row->sub_cat_img;
			$data["catid"] = $row->cat_id;
		}
	}
	
	$this->load->view("backend/admin/header");
	$this->load->view("backend/product/subcategory",$data);
	$this->load->view("backend/admin/footer");
}
public function index($id='')
{
	$data["productlist"] = $this->productmodel->getproductlist();
	$data["categorys"] = $this->productmodel->getcatdetail();
	$data["subcategorys"] = $this->productmodel->getsubcatdetail();
	$data["id"] = $id;
	$data["productname"] = '';
	$data["image"] = '';
	$data["catid"] = '';
	$data["subcatid"] = '';
	$data["productdesc"] = '';
	$data["productcode"] = '';
	$data["productspecification"] = '';
	
	if($id > 0)
	{
		
	$data["catRelatedImages"] = $this->productmodel->getCatRelatedImages($id);
		$productdetail = $this->productmodel->getproductlist($id);
		foreach($productdetail as $row)
		{
			$data["productname"] = $row->product_name;
			$data["catid"] = $row->cat_id;
			$data["subcatid"] = $row->sub_cat_id;
			$data["image"] = $row->product_image;
			$data["productdesc"] = $row->product_description;
			$data["categoryType"] = $row->category_type;
			$data["productcode"] = $row->product_code;
			$data["productspecification"] = json_decode($row->specification);
		}
		
	}
	
	$this->load->view("backend/admin/header");
	$this->load->view("backend/product/product",$data);
	$this->load->view("backend/admin/footer");
}

public function deleteproduct()
{
	$id = $this->input->post("id");
	$res = $this->productmodel->deleteproduct($id);
	$data["iserror"] = FALSE;
	$data["msg"] = 'Product Removed Successfully';
	echo json_encode($data);
	return;
}
public function subcategorylist()
{
	$category = $this->input->post('category');
	$details = $this->productmodel->subcategorylist($category);
	
	$res["iserror"] = FALSE;
	$res["message"] = "";
	$res["data"] = $details;
	
	echo json_encode($res);
}
public function saveproduct()
{
	$id = $this->input->post("id");
	$productname = $this->input->post("productname");
	$category = $this->input->post("category");
	$productcode = $this->input->post("productcode");
	$desc = $this->input->post("desc");
	$subcategory = $this->input->post("subcategory");
	$slug = '';
	/*$oldimage = $this->input->post("oldimage");*/
	/*$oldrelatedimages = $this->input->post('oldrelatedimages');*/
	/*$imgpath = $this->uploadSingleImage('required', '/uploads/product', 'productimg', $oldimage);*/
	$selectedimage = $this->input->post('selectedimage');
	$oldrelatedimages = $this->input->post('oldrelatedimages');
	$productspec = $this->input->post('productspec');
	/*echo $oldrelatedimages;
	exit;*/
	
	if($productname != '' )
	{
		if($id =='' || $id > 0)
		{
			$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $productname);
			$slug = $this->productmodel->checkslug(strtolower($slug),'products','product_slug');
		}
		//Related Images Multiple
		$relatedImgArrDtls = $this->uploadMultipleImage('relatedImages', $selectedimage, 'required', 'uploads', 'category', 'relatedimages', $oldrelatedimages, $id);
		if($relatedImgArrDtls["isError"])
		{
			$data["isError"] = $relatedImgArrDtls["isError"];
			$data["msg"] = $relatedImgArrDtls["msg"];
			echo json_encode($data);
			return;
		}
		$relatedImgArr = $relatedImgArrDtls["imgArr"];
		/*print_r($relatedImgArr);
		exit;*/
		$res = $this->productmodel->saveproduct($id,$productname,$category,$imgpath,$desc,$slug,$productcode,$relatedImgArr,$subcategory,$productspec);
		$data['iserror'] = false;
		if($blogid > 0)
		{
			$data["msg"] = "Product Updated Successfully";
		}
		else{
			$data["msg"] = "Product Created Successfully";
		}
		echo json_encode($data);
		return;
	}
}

public function uploadSingleImage($required = '', $dir, $fileName, $oldImgPath = '')
{
	$imageSrc = "";
	$imagePath = "";
	
	$dirarr = explode("/",$dir);
	$dir = './';
	foreach($dirarr as $dr)
	{
		$dir .= $dr.'/';
		if (!is_dir($dir)) 
		{
		   mkdir($dir);
		}
	}	
	
	$config['upload_path'] = $dir;
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	$config['max_size']	= '5000';
	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	
	$isError = FALSE;
	$errMsg = "";
	
	if(!$this->upload->do_upload($fileName))
	{
		if($oldImgPath == "")
		{
			if($required == "required")
			{
				$isError = TRUE;
				$errMsg = strip_tags($this->upload->display_errors());
			}
		}
		$imageSrc = $oldImgPath;
	}
	else
	{
		if($oldImgPath != "")
		unlink($oldImgPath);
		$data = array('upload_data' => $this->upload->data());
		foreach($data as $row)
		{
			$imagePath = $row["raw_name"]."".$row["file_ext"];
		} 
		$imageSrc = $dir.$imagePath;
		$imageSrc = substr($imageSrc, 2);
	}
	
	$resArr = array();
	$resArr["isError"] = $isError;
	$resArr["msg"] = $errMsg;
	$resArr["imageSrc"] = $imageSrc;
	return $imageSrc;
}

/*Manju Starts*/
public function uploadMultipleImage($fileName, $selectedImgArr, $req, $dir1, $dir2, $dir3, $oldImages, $catId)
{
	$imgArr = array();
	
	$data = array();
	
	$filesCount = count($_FILES[$fileName]['name']);
	
	if($filesCount > 0)
	{
		for($i = 0; $i < $filesCount; $i++)
	    {
	    	$sImgArr = json_decode($selectedImgArr);
	    	for($k=0;$k<count($sImgArr);$k++)
	    	{
				if($_FILES[$fileName]['name'][$i] == $sImgArr[$k])
				{
					$_FILES['userFile']['name'] = $_FILES[$fileName]['name'][$i];
			        $_FILES['userFile']['type'] = $_FILES[$fileName]['type'][$i];
			        $_FILES['userFile']['tmp_name'] = $_FILES[$fileName]['tmp_name'][$i];
			        $_FILES['userFile']['error'] = $_FILES[$fileName]['error'][$i];
			        $_FILES['userFile']['size'] = $_FILES[$fileName]['size'][$i];
			        
			        $newImgArr = $this->uploadSingleImages($req, $dir1, $dir2, $dir3, 'userFile');
					if($newImgArr["isError"])
					{
						$data["isError"] = $newImgArr["isError"];
						$data["msg"] = $newImgArr["msg"];
					}
					else
					{
						$imgArr[$i]["relatedimage"] = $newImgArr["imageSrc"];
					}
				}
			}
	    }
		$newUploadCnt = count($imgArr);
	    $oldImgArr = json_decode($oldImages);
	    for($p=0;$p<count($oldImgArr);$p++)
	    {
			$imgArr[$p + $newUploadCnt]["relatedimage"] = $oldImgArr[$p];
		}
		
		if(count($imgArr) > 0)
		{
			$data["isError"] = FALSE;
			$data["msg"] = "";
		}
	}
	else if($catId != "")
	{
		$oldImages = json_decode($oldImages);
		$newOldImgarr = array();
		for($k=0;$k<count($oldImages);$k++)
		{
			$newOldImgarr[$k]["relatedimage"] = $oldImages[$k];
		}
		$imgArr = $newOldImgarr;
		if(count($imgArr) > 0)
		{
			$data["isError"] = FALSE;
			$data["msg"] = "";
		}
		else
		{
			$data["isError"] = TRUE;
			$data["msg"] = "Please Select atleast One Related Image.";
		}
	}
	$data["imgArr"] = $imgArr;
	return $data;	
}
public function uploadSingleImages($required = '', $dir1, $dir2, $dir3 = '', $fileName, $oldImgPath = '',$catId='')
{
	$imageSrc = "";
	$imagePath = "";
	
	$dir = './'.$dir1.'/';
	if (!is_dir($dir)) 
	{
	   mkdir($dir);
	}
	
	$dir = './'.$dir1.'/'.$dir2.'/';
	if (!is_dir($dir)) 
	{
	   mkdir($dir);
	}
	
	if($dir3 != "")
	{
		$dir = './'.$dir1.'/'.$dir2.'/'.$dir3.'/';
		if (!is_dir($dir)) 
		{
		   mkdir($dir);
		}
	}
	
	$config['upload_path'] = $dir;
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	$config['max_size']	= '5000';
	
	$this->load->library('upload', $config);
	$this->upload->initialize($config);
	
	$isError = FALSE;
	$errMsg = "";
	
	if(!$this->upload->do_upload($fileName))
	{
		if($oldImgPath == "")
		{
			if($required == "required" && $catId == "")
			{
				$isError = TRUE;
				$errMsg = strip_tags($this->upload->display_errors());
			}
		}
		$imageSrc = $oldImgPath;
	}
	else
	{
		if($oldImgPath != "")
		unlink($oldImgPath);
		$data = array('upload_data' => $this->upload->data());
		foreach($data as $row)
		{
			$imagePath = $row["raw_name"]."".$row["file_ext"];
		} 
		$imageSrc = $dir.$imagePath;
		$imageSrc = substr($imageSrc, 2);
	}
	
	$resArr = array();
	$resArr["isError"] = $isError;
	$resArr["msg"] = $errMsg;
	$resArr["imageSrc"] = $imageSrc;
	return $resArr;
}

/*Manju Ends*/

}
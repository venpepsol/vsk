<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {


/*Pradeep starts*/
public function index()
{
	$data["page_title"] = "Chennai Precast - For all your precast concrete needs";
	$data["page_description"] = "Readymade compund wall for an unlimited lifetime at an affordable price. More economical fencing solution than conventional products";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["breadcrumbs"] = FALSE;
	
	/*$data["productlist"] = $this->webmodel->getpropagelist();*/
	$this->load->view('header',$data);
	$this->load->view('home');
	$this->load->view('footer');
}

public function about()
{
	$data["page_title"] = "About Us - Supreme quality precast concrete products manufacturer";
	$data["page_description"] = "We are the leading manufacturer of prefabricated RCC compound walls, industrial godowns, labour quarters, etc. with high quality as per customer requirements.";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "About Us";
	
	
	$this->load->view('header',$data);
	$this->load->view('about');
	$this->load->view('footer');
}

public function gallery()
{
	$data["page_title"] = "Chennai Precast - Products Gallery ";
	$data["page_description"] = "Choose from our wide range of precast concrete products. Customisable designs, low maintenance and high quality products for all your concrete needs.";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Gallery";
	$data["catGallery"] = $this->webmodel->getImgCategoryDetails();
	$data["galleryimg"] = $this->webmodel->getImgDetails();
	
	$this->load->view('header',$data);
	$this->load->view('gallery');
	$this->load->view('footer');
}

public function contact()
{
	$data["page_title"] = "Contact Us - Chennai Precast, Perumbakkam, Chennai";
	$data["page_description"] = "Talk to us and find out what suits your requirements and get quality concrete products for your homes, commercial buildings and industries. Contact us now!";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Contact Us";
	$data["page"] = "Contact Us";
	$this->load->library('recaptcha');
	$data['widget'] = $this->recaptcha->getWidget();
	$data['script'] = $this->recaptcha->getScriptTag();
	$data['js'] = '<script src="'.base_url().'theme/js/jquery.block.js"></script>
					<script src="'.base_url().'theme/js/jquery.ajax.js"></script>';
	
	
	$this->load->view('header',$data);
	$this->load->view('contactus');
	$this->load->view('footer');
}

/*public function products()
{
	$data["page_title"] = "Products - Precast Compound Wall, Kerb Blocks, Concrete Bench & more";
	$data["page_description"] = "We manufacture and supply concrete products like Precast Compund Wall, Kerb Blocks, Fencing Poles ideal for residential, commercial and industrial buildings";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["keywordtag"] = true;
	$data["title"] = "Products";	
	$url="products/";
	$urlstring = $this->uri->uri_string;
		
	$data["productlist"] = $this->webmodel->getpropagelist();
	$this->load->view('header', $data);
	$this->load->view('productwall');
	$this->load->view('footer');
}

public function product($slug = '')
{
	$productlist = $this->webmodel->getprolist($slug);
	$data["productlist"] = $this->webmodel->getpropagelist();
	
	foreach($productlist as $row){
		$data['productTitle'] = $row->product_name;
		$data['images'] = $row->product_image;
		$data['description'] = $row->product_description;
		$data["title"]  = $row->product_name;
		$data["slug"]  = $slug;
		$data["page_title"] = $row->meta_title;
		$data["page_description"] = $row->meta_description;
	}
	
	$data["page_title"] = "Products | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Product";
	
	$this->load->view('header',$data);
	$this->load->view('productwalldetail');
	$this->load->view('footer');
}*/
public function project()
{
	$data["page_title"] = "Project | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('project');
	$this->load->view('footer');
}
public function ongoing()
{
	$data["page_title"] = "Ongoing | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('ongoingproject');
	$this->load->view('footer');
}
public function upcoming()
{
	$data["page_title"] = "Upcoming | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('upcomingproject');
	$this->load->view('footer');
}
public function completed()
{
	$data["page_title"] = "Completed | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('completedproject');
	$this->load->view('footer');
}

public function faq()
{
	$data["page_title"] = "FAQ | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('faq');
	$this->load->view('footer');
}

public function storecontactform()
{
	$data["page_title"] = "FAQ | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	
	$this->load->view('header',$data);
	$this->load->view('faq');
	$this->load->view('footer');
}

public function ashwattha()
{
	$data["page_title"] = "FAQ | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Projectview";
	
	$this->load->view('header',$data);
	$this->load->view('ashwattha');
	$this->load->view('footer');
}

public function aksharam()
{
	$data["page_title"] = "FAQ | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Projectview";
	
	$this->load->view('header',$data);
	$this->load->view('aksharam');
	$this->load->view('footer');
}

public function aishwaryam()
{
	$data["page_title"] = "FAQ | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Projectview";
	
	$this->load->view('header',$data);
	$this->load->view('aishwaryam');
	$this->load->view('footer');
}

public function blog()
{
	$data["page_title"] = "Blog | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Projectview";
	
	$this->load->view('header',$data);
	$this->load->view('blog');
	$this->load->view('footer');
}

public function privacypolicy()
{
	$data["page_title"] = "Privacy | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Privacy Policy";
	$this->load->view('header',$data);
	$this->load->view('privacy');
	$this->load->view('footer');
}

public function termsofuse()
{
	$data["page_title"] = "Privacy | Chennai Precast";
	$data["page_description"] = "Chennai Precast";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["title"] = "Terms of use";
	$this->load->view('header',$data);
	$this->load->view('terms');
	$this->load->view('footer');
}

public function keyword()
{
	$urlstring = $this->uri->uri_string;	
	$pieces = explode("/", $urlstring);
	$piecescount = count($pieces);
	$keytitle = '';
	for($i=1;$i<$piecescount;$i++)
	{
		$page_titleKey = $pieces[$i];
		if($pieces[$i] == 'keyword')
		{
			if($piecescount > ($i+1))
			{
				$slug = $pieces[$i+1];
				$keytitle = $pieces[$i-1];
			}
		}
	}
	$page_titleKey = ucfirst(str_replace('-',' ',$page_titleKey));

	/*$keywordTitle = $this->webmodel->getKeywordDetail();
	$keywordState = $this->webmodel->getKeywordstateDetail();
	foreach($keywordState as $row){
		$keystate = $row->state_slug;
		if (strpos($keytitle, $keystate) !== false)
		{
			$tagname = str_replace( $keystate, '<state>', $keytitle);
			$keywordstatename = $row->statename;
		}
	}
	foreach($keywordTitle as $row){
		$keyswordslug= $row->page_keyword_slug;
		if (strpos($keytitle, $keyswordslug) !== false)
		{
			$keywordtag = str_replace( $keyswordslug, '<services>', $tagname);
			$keywordTitltname = $row->page_keyword;
		}
	}
	
	$pageDescription = $this->webmodel->getkeywordDesc($keywordtag);
	foreach($pageDescription as $res){
		$desc = $res->description;
	}
	$pagedesc = str_replace('<services>',$keywordTitltname,$desc);
	$pagedesc = str_replace('<state>',$keywordstatename,$pagedesc);
	
	$pagetitle = str_replace('<services>',$keywordTitltname,$keywordtag);
	$pagetitle = str_replace('<state>',$keywordstatename,$pagetitle);
	$pagetitle = str_replace('-',' ',$pagetitle);

	

	$keywordDetail = $this->webmodel->getKeywordDetail($slug);
	$keytitle = $keywordDetail[0]->page_keyword;
	$data["page_title"] = $pagetitle;
	$data["keyword_title"] = $keytitle;
	$data["keywordPageSlug"] = $keywordDetail[0]->page_keyword_slug;
	$data["keyword_slug"] = $slug;*/
	$data["page_title"] = $page_titleKey;
	$data["page_description"] = "We manufacture and supply concrete products like Precast Compund Wall, Kerb Blocks, Fencing Poles ideal for residential, commercial and industrial buildings";
	$data["page_keywords"] = "drainage cover slab, kerb blocks, labour quarters, precast compound wall, concrete bench, baluster, tree guards, readymade compound wall, readymade compound wall price, stair balusters, precast compound wall cost, precast compound wall price, readymade compound wall manufacturer, rcc compound wall cost, precast concrete compound wall, outdoor concrete bench, precast compound wall near me, block paving kerbs, fencing pole cost, readymade concrete compound wall, precast compound wall panels, block paving kerb stones, readymade cement compound, tree guard protection, precast boundary wall, picket fence, small picket fence, chennai precast, Precast concrete products";
	$data["breadcrumbs"] = FALSE;
	/*$data["page_description"] = $pagedesc;*/

	$data["title"] = "Product";
	$data["keywordtag"] = true;
	
	$data["productlist"] = $this->webmodel->getpropagelist();
	$this->load->view('header', $data);
	$this->load->view('productwall');
	$this->load->view('footer');
}
/*Pradeep Ends*/
}

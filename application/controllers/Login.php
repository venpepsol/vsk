<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
public function __construct()
{
	parent::__construct();

	$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
	$this->output->set_header('Pragma: no-cache');
	
	if(($this->session->userdata('userid') != null) || ($this->session->userdata('userid') != ""))
	{
		redirect(base_url().'admin');
	}
	
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
}
public function index()
{
	$this->load->view("backend/login/loginpage");
}	
public function checklogin()
{
	$username = $this->input->post("username");
	$password = $this->input->post("password");
	
	if($username == ''|| $password == '')
	{
		$data["iserror"] = true;
		$data["msg"] = "Please Fill Required Fields";
		echo json_encode($data);
		return;
	}
	$res = $this->loginmodel->checklogin($username,$password);
	/*print_r($res);
	exit();*/
	echo json_encode($res);
	return;
}
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	/*Rubha starts*/
public function __construct()
{
	parent::__construct();

	$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
	$this->output->set_header('Pragma: no-cache');
	
	if(($this->session->userdata('userid') == null) || ($this->session->userdata('userid') == ""))
	{
		redirect(base_url().'login');
	}
}
public function index()
{
	$data["contactus"] = $this->webmodel->getcontactus();
	$data["casestudy"] = $this->webmodel->getcasestudys();
	
	$this->load->view('backend/header');
	$this->load->view('backend/dashboard',$data);
	$this->load->view('backend/footer');
}
public function logout()
{
	$userData = array();
	$this->session->set_userdata($userData);
	$this->session->sess_destroy();
	$this->load->helper('cookie');
	delete_cookie('ci_transmart');
	redirect(base_url().'login');
}

public function changepassword()
{
	$this->load->view('backend/changepassword');
}

public function updatePassword()
{
	$oldPassword = $this->input->post('oldPassword');
	$newPassword = $this->input->post('newPassword');
	$userId = $this->session->userdata('userid');
	$result = $this->webmodel->getUserDetails($userId);
	foreach($result as $row)
	{
		$checkPassword = $row->password;
	}
	if($checkPassword != md5($oldPassword))
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Your Old Password is Wrong. Please Check.";
	}
	else
	{
		$this->webmodel->updateUserPassword($userId, $newPassword);
		$data["isError"] = FALSE;
		$data["msg"] = "Password Updated Successfully...";
	}
	echo json_encode($data);
}
public function casestudy($entryId = '')
{
	$data["entryId"] = $entryId;
	
	$data["Title"] = '';
	$data["Industry"] = '';
	$data["BuildupArea"] = '';
	$data["Problem"] = '';
	$data["Analysis"] = '';
	$data["Solution"] = '';
	$data["imgurl"] = '';
	$data["Description"] = '';
	
	$res = $this->webmodel->getcasestudies($entryId);
	
	if($entryId > 0)
	{
		foreach($res as $row)
		{
			$data["Title"] = $row->title;
			$data["Industry"] = $row->industry;
			$data["BuildupArea"] = $row->build_up_area;
			$data["Problem"] = $row->problem;
			$data["Analysis"] = $row->analysis;
			$data["Solution"] = $row->solution;
			$data["imgurl"] = $row->img;
			$data["Description"] = $row->description;
		}
	}
	else
	{
		$data["case"] = $res;
	}
	
	$this->load->view('backend/header');
	$this->load->view('backend/casestudy', $data);
	$this->load->view('backend/footer');
}

public function savecasestudy()
{
	$entryId = $this->input->post('entryId');
	$Title = $this->input->post('Title');
	$Industry = $this->input->post('Industry');
	$BuildupArea = $this->input->post('BuildupArea');
	$Problem = $this->input->post('Problem');
	$Analysis = $this->input->post('Analysis');
	$Solution = $this->input->post('Solution');
	$Description = $this->input->post('Description');
	$oldimage = $this->input->post("oldimage");
	$pageimagechanged = $this->input->post("pageimagechanged");
	$slug = '';
	if($Title != "")
	{
		if($pageimagechanged == 'yes')
		{
			if($oldimage != '')
			{
				if(file_exists($oldimage))
				{
					unlink($oldimage);
				}
			}
			$imgpath = $this->imageuploads('pageimg');
		}
		else
		{
			$imgpath = $oldimage;
		}
		if($entryId =='' || $entryId >0)
		{
			$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $Title);
			$slug = $this->productmodel->checkslug(strtolower($slug),'case_study','case_slug');
			
		}	
		$this->webmodel->savecasestudy($entryId, $Title, $Industry, $BuildupArea, $Problem, $Analysis, $Solution, $Description,$slug,$imgpath);
		
		$data["isError"] = FALSE;
		if($entryId > 0)
		{
			$data["msg"] = "Entry Updated Successfully.";
		}
		else
		{
			$data["msg"] = "Entry Saved Successfully.";
		}
	}
	else
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill All Details.";
	}
	echo json_encode($data);
}

public function module($entryId = '')
{
	$data["entryId"] = $entryId;
	
	$data["CaseId"] = '';
	$data["Description"] = '';
	$data["TotalCost"] = '';
	$data["case"] = $this->webmodel->getcasestudies();
	$res = $this->webmodel->getmodule($entryId);
	
	if($entryId > 0)
	{
		foreach($res as $row)
		{
			$data["CaseId"] = $row->id;
			$data["Description"] = $row->module_desc;
			$data["TotalCost"] = $row->total_cost;
		}
	}
	else
	{
		$data["module"] = $res;
	}
	
	$this->load->view('backend/header');
	$this->load->view('backend/module', $data);
	$this->load->view('backend/footer');
}

public function savemodule()
{
	$entryId = $this->input->post('entryId');
	$CaseId = $this->input->post('CaseId');
	$Description = $this->input->post('Description');
	$TotalCost = $this->input->post('TotalCost');
	
	
	if($CaseId != "")
	{
		
		$this->webmodel->savemodule($entryId, $CaseId, $Description, $TotalCost);
		
		$data["isError"] = FALSE;
		if($entryId > 0)
		{
			$data["msg"] = "Entry Updated Successfully.";
		}
		else
		{
			$data["msg"] = "Entry Saved Successfully.";
		}
	}
	else
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill All Details.";
	}
	echo json_encode($data);
}
public function delEntry()
{
	$entryId = $this->input->post('entryId');
	$tableName = $this->input->post('tableName');
	$columnName = $this->input->post('columnName');
	
	if($entryId > 0 && $tableName != "" && $columnName != "")
	{
		$this->webmodel->delEntry($entryId, $tableName, $columnName);
		
		$data["isError"] = FALSE;
		$data["msg"] = "Entry Removed Successfully.";
	}
	else
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill All Details.";
	}
	echo json_encode($data);
}
public function imageuploads($imgfile)
{
	$dir ='./uploads/';
	if(!is_dir($dir))
	{
		mkdir($dir);
	}
	$config["upload_path"] = $dir;
	$config["allowed_types"] = "gif|jpg|png|jpeg";
	$config["max_size"] = '5000';
	$this->load->library("upload",$config);
	$this->upload->initialize($config);
	
	if(!$this->upload->do_upload($imgfile))
	{
		$error = array("error"=> $this->upload->display_errors());
		print_r($this->upload->display_errors());
		return;
	}
	else
	{
		$data = array('upload_data'=> $this->upload->data());
		foreach($data as $row)
		{
			$imagepath = $row['raw_name']."".$row['file_ext'];
		}
		$imagesrc ='uploads/'.$imagepath;
	}
	$imgpath = $imagesrc;
	return $imgpath;
}

	/*Rubha Ends*/












}
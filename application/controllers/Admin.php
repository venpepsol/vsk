<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
public function __construct()
{
	parent::__construct();

	$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
	$this->output->set_header('Pragma: no-cache');
	
	if(($this->session->userdata('userid') == null) || ($this->session->userdata('userid') == ""))
	{
		redirect(base_url().'login');
	}
	
	error_reporting(E_ALL ^ (E_NOTICE | E_WARNING | E_DEPRECATED));
}
public function index()
{
	$data["contactform"] = $this->webmodel->getcontactdetails();
	$data["catlist"] = $this->productmodel->getcatdetail();
	$data["viewList"] = '';
	$data["prodcnt"] = $this->webmodel->getProduct();
	$data["catcnt"] = $this->webmodel->getcategoryproduct();
	$data["viewcnt"] = '';
	
	$this->load->view("backend/admin/header",$data);
	$this->load->view("backend/admin/dashboard");
	$this->load->view("backend/admin/footer");
}
public function users($userId='')
{
	$data["userId"] = $userId;
	$data["userList"] = $this->adminmodel->getusers();
	$userDetail = $this->adminmodel->getusers($userId);
	$data["userTitle"] = '';
	$data["description"] = '';
	if($userId > 0)
	{
		foreach($userDetail as $row)
		{
			$data["userTitle"] = $row->firstname;
			$data["email"] = $row->email;
			$data["gender"] = $row->gender;
			$data["userType"] = $row->type;
			$data["password"] = '';
		}
	}
	
	$this->load->view("backend/admin/header");
	$this->load->view("backend/admin/users",$data);
	$this->load->view("backend/admin/footer");
}
public function deleteusers()
{
	$postId = $this->input->post("postId");
	$res = $this->adminmodel->deleteusers($postId);
	$data["isError"] = FALSE;
	$data["msg"] = 'User Removed Successfully';
	echo json_encode($data);
	return;
}
public function saveusers()
{
	$userId = $this->input->post("userId");
	$userTitle = $this->input->post("userTitle");
	$email = $this->input->post("email");
	$password = $this->input->post("password");
	$gender = $this->input->post("gender");
	$usertype = $this->input->post("usertype");
	
	if($userTitle != "" && $email != "")
	{
		$res = $this->adminmodel->saveusers($userId, $userTitle, $email,$password,$gender,$usertype);
		$data["isError"] = FALSE;
		if($userId > 0)
		$data["msg"] = "User Updated Successfully";
		else
		$data["msg"] = "User Saved Successfully";
	}
	else{
		$data["isError"] = TRUE;
		$data["msg"] = "Please Fill Required Fields";
	}
	echo json_encode($data);
}
public function changepassword()
{
	$this->load->view('backend/admin/changepassword');
}
public function updatePassword()
{
	$oldPassword = $this->input->post('oldPassword');
	$newPassword = $this->input->post('newPassword');
	$userId = $this->session->userdata('userid');
	$result = $this->adminmodel->getUserDetails($userId);
	foreach($result as $row)
	{
		$checkPassword = $row->password;
	}
	if($checkPassword != md5($oldPassword))
	{
		$data["isError"] = TRUE;
		$data["msg"] = "Your Old Password is Wrong. Please Check.";
	}
	else
	{
		$this->adminmodel->updateUserPassword($userId, $newPassword);
		$data["isError"] = FALSE;
		$data["msg"] = "Password Updated Successfully...";
	}
	echo json_encode($data);
}
public function logout()
{
	$userData = array();
	$this->session->set_userdata($userData);
	$this->session->sess_destroy();
	$this->load->helper('cookie');
	delete_cookie('ci_spacemanagement');
	redirect(base_url().'login');
}
public function checkuser()
{
	$email = $this->input->post("email");
	
	if($email == '')
	{
		$data["iserror"] = true;
		$data["msg"] = "Please Fill Required Fields";
		echo json_encode($data);
		return;
	}
	$res = $this->loginmodel->checklogin($username,$password);
	/*print_r($res);
	exit();*/
	echo json_encode($res);
	return;
}
}
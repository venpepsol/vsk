<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Productmodel extends CI_Model{
	
	
public function getcatdetail($catid='',$subcat='')
{
	$sql = "SELECT * FROM category WHERE STATUS = 'active'";
	if($catid !='')
	{
		$sql .= " AND cat_id = '".$catid."'";
	}
	if($subcat !='')
	{
		$sql .= " AND category_type = '".$subcat."'";
	}
	$res = $this->db->query($sql);
	return $res->result();
}	
	
public function getsubcatdetail($catid='')
{
	$sql = "SELECT s.*,c.* 
			FROM sub_category s
			INNER JOIN category c ON s.cat_id = c.cat_id
			WHERE c.status = 'active' AND s.status = 'active'";
	if($catid !='')
	{
		$sql .= " AND s.sub_cat_id = '".$catid."'";
	}
	$res = $this->db->query($sql);
	return $res->result();
}
public function subcategorylist($catid='')
{
	$sql = "SELECT s.*, c.category_type 
			FROM sub_category s
				INNER JOIN category c ON c.cat_id = s.cat_id
			 WHERE s.status = 'active'";
	if($catid !='')
	{
		$sql .= " AND s.cat_id = '".$catid."'";
	}
	$res = $this->db->query($sql);
	return $res->result();
}
public function getproductlist($id='')
{
	$sql = "SELECT 
			p.product_id, p.product_slug, p.product_code, p.product_name,p.specification, 
			p.cat_id, c.cat_slug, c.category_name, 
			p.product_image, p.product_description,p.sub_cat_id,s.sub_cat_name, 
			DATE_FORMAT(p.created_on,'%b %d, %Y') AS createdon
		FROM 
			products p 
			INNER JOIN category c ON p.cat_id = c.cat_id
			LEFT JOIN sub_category s ON s.sub_cat_id = p.sub_cat_id
			
		WHERE 
			p.status <> 'inactive' AND c.status <> 'inactive'";
	if($id !='')
	{
		$sql .= " AND p.product_id = $id";
	}
	/*echo $sql;
	exit;*/
	$res = $this->db->query($sql);
	return $res->result();
}	
public function deletecategory($catid)
{
	$sql = "update category set status = 'inactive' where cat_id = $catid";
	$res = $this->db->query($sql);
}
public function deletesubcategory($catid)
{
	$sql = "update sub_category set status = 'inactive' where sub_cat_id = $catid";
	$res = $this->db->query($sql);
}	
public function deleteproduct($id)
{
	$sql = "update products set status = 'inactive' where product_id = $id";
	$res = $this->db->query($sql);
}
public function savecategory($catname,$categorydescrption,$catid='',$slug,$imgpath,$categoryType,$categoryorder)
{
	if($catid != '')
	{
		$sql = "update category set category_name = '".$catname."', description = '".$categorydescrption."', cat_img = '".$imgpath."', category_type = '".$categoryType."', order_no = '".$categoryorder."' where cat_id = '".$catid."'";
	$res = $this->db->query($sql);
	}
	else
	{		
		$sql = "insert into category set category_name = '".$catname."',description = '".$categorydescrption."', cat_slug = '".$slug."', cat_img = '".$imgpath."', category_type = '".$categoryType."', order_no = '".$categoryorder."'";
		$res = $this->db->query($sql);
		
	}
}
public function savesubcategory($categoryid,$subcategoryname,$subcatid='',$slug,$imgpath)
{
	if($subcatid != '')
	{
		$sql = "update sub_category set sub_cat_name = '".$subcategoryname."', sub_cat_img = '".$imgpath."',cat_id = '".$categoryid."' where sub_cat_id = '".$subcatid."'";
		/*echo $sql;
		exit;*/
	$res = $this->db->query($sql);
	}
	else
	{		
		$sql = "insert into sub_category set sub_cat_name = '".$subcategoryname."', sub_cat_slug = '".$slug."', sub_cat_img = '".$imgpath."',cat_id = '".$categoryid."'";
		/*echo $sql;
		exit;*/
		$res = $this->db->query($sql);
	}
}
public function saveproduct($id,$productname,$category,$imgpath,$desc,$slug,$productcode,$relatedImgArr,$subcategory,$productspec)
{
	if($id != '')
	{
		$sql = "update products set product_name = '".$productname."',cat_id = '".$category."',product_image = '".$imgpath."', product_description = '".$this->db->escape_str($desc)."',product_code = '".$productcode."',sub_cat_id = '".$subcategory."', specification = '".$productspec."' where product_id = '".$id."'";
		/*echo $sql;
		exit;*/
		$this->db->query($sql);
		$sqlDel = "DELETE FROM cat_relatedimages WHERE catid = $id";
		$this->db->query($sqlDel);
		foreach($relatedImgArr as $row)
		{
			$sql = "INSERT INTO cat_relatedimages SET 
						catid = $id, 
						imagepath = '".$row["relatedimage"]."'";
			$this->db->query($sql);
			echo $sql;
		}
	}
	else
	{		
		$sql = "insert into products set product_name = '".$productname."',cat_id = '".$category."',product_image = '".$imgpath."', product_code = '".$productcode."', product_description = '".$this->db->escape_str($desc)."', product_slug = '".$slug."',sub_cat_id = '".$subcategory."', specification = '".$productspec."'";
		$this->db->query($sql);
		$id = $this->db->insert_id();
		/*print_r($relatedImgArr);
		exit;*/
		foreach($relatedImgArr as $row)
		{
			$sql = "INSERT INTO cat_relatedimages SET 
						catid = $id, 
						imagepath = '".$row["relatedimage"]."'";
			$this->db->query($sql);
			/*echo $sql;
			exit;*/
		}
		
	}
	
	
}
public function getCatRelatedImages($catId)
{
	$sql = "SELECT *
			FROM cat_relatedimages
			WHERE catid = $catId";
	$res = $this->db->query($sql);
	return $res->result();
}

public function getSlugName($slug, $table, $column)
{
	$sql = "SELECT $column FROM $table WHERE $column = '".$slug."'";
	$res = $this->db->query($sql);
	$slugName = '';
	if(count($res) > 0)
	{
		foreach($res->result() as $row)
		{
			$slugName = $row->$column;
		}
	}
	return $slugName;
}

/*Manju Starts*/

public function getTagsDetails()
{
	$sql = "SELECT * FROM tags WHERE 1=1";
	$res = $this->db->query($sql);
	return $res->result();
}

public function checkslug($slug, $table, $column)
{
	 $success = 0;
	 $newslug = $this->getSlugName($slug, $table, $column);
	 if($newslug!='')
	 {
		$strLen = strlen($newslug);
		if(intval($newslug[$strLen-1])>0)
		{   
			$newNumber = intval($newslug[$strLen-1]) +1;
			$newslug = substr($newslug, 0, -1).$newNumber;
		}
		else
		{  
			$newslug .= "-1";
		}
		return $this->checkslug($newslug, $table, $column);
	 }
	 else
	 {
		return $slug; 
	 }
}

/*Manju Ends*/

}
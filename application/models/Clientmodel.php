<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Clientmodel extends CI_Model{
	
/*Manju Starts*/

public function getClientDetails($clientId = '', $start = '', $perpage = '')
{
	$sql = "SELECT * FROM clients WHERE status <> 'inactive'";
	if($clientId > 0)
	{
		$sql .= " AND id = $clientId";
	}
	if($start != "" || $perpage != "")
	{
		$sql .= " LIMIT $start, $perpage";
	}
	$res = $this->db->query($sql);
	return $res->result();
}

public function saveClient($clientId, $clientName, $clientCity, $clientState, $clientImage)
{
	if($clientId > 0)
	{
		$sql = "UPDATE clients SET 
					client_name = '".$clientName."', city = '".$clientCity."', 
					state = '".$clientState."', clientimg = '".$clientImage."', 
					modified_on = NOW(), 
					modified_by = '".$this->session->userdata('userid')."'
				WHERE id = $clientId";
		$this->db->query($sql);
	}
	else
	{
		$sql = "INSERT INTO clients SET 
					client_name = '".$clientName."', city = '".$clientCity."', 
					state = '".$clientState."', clientimg = '".$clientImage."', 
					created_on = NOW(), 
					created_by = '".$this->session->userdata('userid')."'";
		$this->db->query($sql);
	}
}

public function deleteClient($clientId)
{
	$sql = "UPDATE clients SET status = 'inactive' WHERE id = $clientId";
	$this->db->query($sql);
}

/*Manju Ends*/

}